(function($) {
"use strict";

/*------------------------------------------------------------------
[Table of contents]


-------------------------------------------------------------------*/


$(window).on('load', function() {

}); // END load Function 

$(document).ready(function() {
	$(function() {
		$('#menu').cookcodesmenu({
			brand: ' '
		});
    });
    if ($('.background-image').length > 0) {
        $('.background-image').each(function () {
            var src = $(this).attr('data-src');
            $(this).css({
                'background-image': 'url(' + src + ')'
            });
        });
    }

    if ($('.testimonial-slider.owl-carousel').length > 0) {
        $('.testimonial-slider.owl-carousel').owlCarousel({
            loop: true,
            items: 3,
            autoplay: true,
            nav: false,
            dots: true,
            navText: ['<i class="fa fa-plus" aria-hidden="true"></i>', '<i class="fa fa-plus" aria-hidden="true"></i>'],
            margin:30,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    margin:0
                },
                600:{
                    items:1,
                    margin:0
                },
                991:{
                  items:2,
                },
                1200:{
                  items:3
                }
            }
        });
    }
    $('.popup-link').magnificPopup({
        type: 'image',
        gallery: {
              enabled: true,
              navigateByImgClick: true,
              preload: [0,1]
            },	
        });
        if ($('.video-popup').length > 0) {
          $('.video-popup').magnificPopup({
              disableOn: 700,
              type: 'iframe',
              mainClass: 'mfp-fade',
              removalDelay: 160,
              preloader: false,
              fixedContentPos: false
          });
      }


}); // end ready function

$(window).on('scroll', function() {

}); // END Scroll Function 

$(window).on('resize', function() {

}); // End Resize

})(jQuery);