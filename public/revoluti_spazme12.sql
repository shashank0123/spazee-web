-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 02, 2019 at 10:14 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `revoluti_spazme12`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

DROP TABLE IF EXISTS `about`;
CREATE TABLE IF NOT EXISTS `about` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `about`, `created_at`, `updated_at`) VALUES
(1, '<p><br />\r\n<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remain</p>', '2019-07-30 15:00:48', '2019-07-31 18:29:33'),
(2, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2019-07-31 12:25:52', '2019-07-31 12:25:52'),
(3, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2019-07-31 12:29:23', '2019-07-31 12:29:23'),
(4, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2019-07-31 12:30:35', '2019-07-31 12:30:35'),
(5, '<p><strong>Jyotika Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2019-07-31 18:24:41', '2019-07-31 18:24:41');

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completeAddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `landmark` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saveAs` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `user_id`, `location`, `completeAddress`, `landmark`, `pincode`, `city`, `state`, `saveAs`, `created_at`, `updated_at`) VALUES
(1, '1', 'punjab', 'Punjab, Firozepur', 'uttam nagar', '152004', 'Firozepur,152004', 'hello', 'Home', '2019-06-25 11:45:39', '2019-06-25 11:45:39'),
(4, '28', 'punjab', 'Punjab, Firozepur', 'uttam nagar', '162004', 'Firozepur', 'punjab', 'office', '2019-06-25 01:59:17', '2019-06-25 01:59:17'),
(6, '1', 'kela devi', 'jawahan nagar square', 'oppo. raja tower', '455001', 'Dewas', 'MP', 'Office', '2019-06-25 11:59:19', '2019-06-25 11:59:19'),
(13, '2', 'Indore', 'High Court, Indore, 455001', 'HIGH COURT', '452010', 'Indore', 'MP', 'Other', '2019-06-26 10:15:59', '2019-06-26 10:15:59'),
(14, '2', 'dewas', 'dewas', 'dewas', '455001', 'dewas', 'MP', 'Other', '2019-07-19 03:58:51', '2019-07-19 03:58:51'),
(15, '38', 'uttam nagar', 'uttam', 'billu', '76788766', '678776', 'ghhgg', 'Office', '2019-07-19 17:21:03', '2019-07-19 17:21:03'),
(16, '41', 'delhi', 'utaam nagar delhi billu building', 'ram road', '4578', 'billu', 'delhi', 'Office', '2019-07-19 20:16:41', '2019-07-19 20:16:41'),
(17, '42', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'Other', '2019-07-20 03:19:13', '2019-07-20 03:19:13'),
(18, '42', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'Other', '2019-07-20 03:19:14', '2019-07-20 03:19:14'),
(20, '43', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'Other', '2019-07-20 04:42:28', '2019-07-20 04:42:28'),
(21, '35', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'Other', '2019-07-20 05:24:46', '2019-07-20 05:24:46'),
(22, '35', 'uttam', 'billu', 'building', '456123', 'uttam', 'ndls', 'Other', '2019-07-20 05:38:09', '2019-07-20 05:38:09'),
(23, '44', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', '2019-07-20 12:01:50', '2019-07-20 12:01:50'),
(24, '46', 'Uttam Nagar', 'Uttam nagar   Delhi', 'Billu building', '679965', 'Uttam Nagar', 'Delhi', 'Office', '2019-07-20 12:51:42', '2019-07-20 12:51:42'),
(25, '47', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'Office', '2019-07-20 13:56:05', '2019-07-20 13:56:05'),
(26, '56', 'Utttam', 'Bullu', 'Building', '76777', 'Uttam', 'Delhi', 'Other', '2019-07-20 21:26:15', '2019-07-20 21:26:15'),
(27, '59', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'Other', '2019-07-21 00:04:00', '2019-07-21 00:04:00'),
(28, '53', 'hjjhk', 'hjhkhk', 'hkjhkk', '8907', 'hkhjj', 'gjghjj', 'Office', '2019-07-23 12:26:45', '2019-07-23 12:26:45'),
(29, '62', 'jkkkk', 'kkkkkkk', 'llllll', '78899', 'jnjkkkk', 'ndls', 'Office', '2019-07-23 12:31:18', '2019-07-23 12:31:18'),
(30, '65', 'D2 68 jeewan park 8', 'D2 68 JEEWAN PARK5', 'Friday MArket', '110059', 'New Delhi', 'Delhi', 'Home', '2019-07-23 13:02:18', '2019-07-23 13:02:18'),
(31, '60', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'Home', '2019-07-26 17:52:10', '2019-07-26 17:52:10'),
(32, '69', '110059', 'RZ-D-10, Mahindra Park, Uttam Nagar, Uttam Nagar, Uttam Nagar', 'Del', 'undefined', 'West Delhi', 'undefined', 'Home', '2019-07-26 17:58:52', '2019-07-26 17:58:52'),
(33, '69', '110059', 'RZ-D-10, Mahindra Park, Uttam Nagar, Uttam Nagar, Uttam Nagar', 'Del', 'undefined', 'West Delhi', 'undefined', 'Office', '2019-07-26 17:59:02', '2019-07-26 17:59:02'),
(34, '70', '110059', 'RZ-D-10, Mahindra Park, Uttam Nagar, Uttam Nagar, Uttam Nagar', 'Del', 'undefined', 'West Delhi', 'undefined', 'Office', '2019-07-26 18:03:54', '2019-07-26 18:03:54'),
(35, '60', 'undefined', 'errr', 'undefined', 'undefined', 'undefined', 'undefined', 'Other', '2019-07-27 15:56:42', '2019-07-27 15:56:42'),
(36, '71', 'delhi', 'delhi', 'delhi', '110035', 'delhi', 'delhi', 'Home', '2019-07-27 17:31:50', '2019-07-27 17:31:50');

-- --------------------------------------------------------

--
-- Table structure for table `appoinment`
--

DROP TABLE IF EXISTS `appoinment`;
CREATE TABLE IF NOT EXISTS `appoinment` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Brand_Name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Brand_Model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Brand_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `reason_newappoin` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `track_point` enum('Order_placed','Confirm_Order','agent_assign','delivery') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `appoinment`
--

INSERT INTO `appoinment` (`id`, `user_id`, `username`, `email`, `phone`, `Brand_Name`, `Brand_Model`, `image`, `Brand_color`, `price`, `address`, `pincode`, `service_date`, `service_time`, `field`, `order_id`, `created_at`, `updated_at`, `status`, `reason_newappoin`, `otp`, `track_point`) VALUES
(2, 1, 'vikas', 'v8.vikas@gmail.com', '9815494879', 'Google', 'Google Nexus 5', '1561357760.jpg', 'black', 'RS 9900', 'kela devi jawahar nagar dewas, MP 455001', '455001', '26 Jun', '2019-06-26 01:49:25', 'Office', '653714', '2019-06-26 08:49:25', '2019-06-26 08:49:25', 0, NULL, 611689, 'Order_placed'),
(10, 2, 'vikas', 'v8.vikas@gmail.com', '8770521331', 'Google', 'Google Nexus 5', '1561357760.jpg', 'black', 'RS 9900', 'High Court, Indore, 455001', '452010', '27 Jun', '2019-06-27 05:31:21', 'Other', '331034', '2019-06-27 12:31:21', '2019-06-27 12:31:21', 0, NULL, 832981, 'Order_placed'),
(11, 2, 'vikas', 'v8.vikas@gmail.com', '8770521331', 'MI', 'Mi pro', '1561357814.jpg', 'white', 'RS 9900', 'High Court, Indore, 455001', '452010', '27 Jun', '2019-06-27 05:58:27', 'Other', '676448', '2019-06-27 12:58:27', '2019-06-27 12:58:27', 0, NULL, 756827, 'Order_placed'),
(12, 2, 'vikas', 'v8.vikas@gmail.com', '8959893980', 'Google', 'Google Nexus 5', '1561357760.jpg', 'black', 'RS 9900', 'High Court, Indore, 455001', '452010', '27 Jun', '2019-06-27 06:39:30', 'Other', '255201', '2019-06-27 13:39:30', '2019-06-27 13:39:30', 0, NULL, 417650, 'Order_placed'),
(13, 2, 'vikas', 'v8.vikas@gmail.com', '8959893980', 'Google', 'Google Nexus 5', '1561357760.jpg', 'black', 'RS 9900', 'High Court, Indore, 455001', '452010', '27 Jun', '2019-06-27 16:42:01', 'Other', '855242', '2019-06-27 23:42:01', '2019-06-27 23:42:01', 0, NULL, NULL, 'Order_placed'),
(14, 2, 'vikas', 'v8.vikas@gmail.com', '8959893980', 'Google', 'Google Nexus 5', '1561357760.jpg', 'black', 'RS 9900', 'High Court, Indore, 455001', '452010', '30 Jun', '2019-06-30 22:31:10', 'Other', '640976', '2019-07-01 05:31:10', '2019-07-01 05:31:10', 0, NULL, NULL, 'Order_placed'),
(15, 2, 'vikas', 'v8.vikas@gmail.com', '8959893980', 'Google', 'Google Nexus 5', '1561357760.jpg', 'black', 'RS 9900', 'High Court, Indore, 455001', '452010', '30 Jun', '2019-06-30 22:34:08', 'Other', '523658', '2019-07-01 05:34:08', '2019-07-01 05:34:08', 0, NULL, NULL, 'Order_placed'),
(16, 2, 'vikas', 'v8.vikas@gmail.com', '8959893980', 'Google', 'Google Nexus 5', '1561357760.jpg', 'black', 'RS 9900', 'High Court, Indore, 455001', '452010', '30 Jun', '2019-06-30 22:35:34', 'Other', '279299', '2019-07-01 05:35:34', '2019-07-01 05:35:34', 0, NULL, NULL, 'Order_placed'),
(17, 38, 'nnnn', 'nnnn@gmail.com', '1212121212', 'MI', 'Mi pro', '1561357814.jpg', 'white', 'RS 9900', 'uttam', '76788766', '24Jul', '2019-07-19 10:26:43', 'Office', '998746', '2019-07-19 17:26:43', '2019-07-19 17:26:43', 0, NULL, NULL, 'Order_placed'),
(18, 41, 'sdfghjkl', 's@gmail.com', '8528528521', 'MI', 'Mi pro', '1561357814.jpg', 'white', 'RS 9900', 'utaam nagar delhi billu building', '4578', '21Jul', '2019-07-19 13:17:16', 'Office', '681186', '2019-07-19 20:17:16', '2019-07-19 20:17:16', 0, NULL, 741255, 'Order_placed'),
(20, 43, 'nnnkkk', 'v@gmail.com', '5555555555', 'SAMSUNG', 'Galexy s7', '1561357861.jpg', 'black', 'RS 9900', 'undefined', 'undefined', '24Jul', '2019-07-19 21:42:43', 'Other', '541049', '2019-07-20 04:42:43', '2019-07-20 04:42:43', 0, NULL, NULL, 'Order_placed'),
(21, 35, 'vk', 'vk@gmail.com', '2222222222', 'SAMSUNG', 'Galexy s7', '1561357861.jpg', 'black', 'RS 9900', 'undefined', 'undefined', '24Jul', '2019-07-19 22:24:58', 'Other', '847709', '2019-07-20 05:24:58', '2019-07-20 05:24:58', 0, NULL, NULL, 'Order_placed'),
(22, 35, 'vk', 'vk@gmail.com', '2222222222', 'SAMSUNG', 'Galexy s7', '1561357861.jpg', 'black', 'RS 9900', 'billu', '456123', '24Jul', '2019-07-19 22:39:29', 'Other', '198555', '2019-07-20 05:39:29', '2019-07-20 05:39:29', 0, NULL, NULL, 'Order_placed'),
(23, 44, 'lll', 'l@gmail.com', '22222222222', 'SAMSUNG', 'Galexy s7', '1561357861.jpg', 'black', 'RS 9900', 'undefined', 'undefined', '25Jul', '2019-07-20 05:02:02', 'undefined', '534855', '2019-07-20 12:02:02', '2019-07-20 12:02:02', 0, NULL, NULL, 'Order_placed'),
(24, 46, 'Kkkk', 'Kkk@gmail.com', '9090909090', 'SAMSUNG', 'Galexy s7', '1561357861.jpg', 'black', 'RS 9900', 'Uttam nagar   Delhi', '679965', '23Jul', '2019-07-20 05:53:12', 'Office', '866637', '2019-07-20 12:53:12', '2019-07-20 12:53:12', 0, NULL, 346650, 'Order_placed'),
(25, 47, 'Not in bhai', 'N@gmail.com', '999999998', 'SAMSUNG', 'Galexy s7', '1561357861.jpg', 'black', 'RS 9900', 'undefined', 'undefined', '23Jul', '2019-07-20 06:56:27', 'Office', '177083', '2019-07-20 13:56:27', '2019-07-20 13:56:27', 0, NULL, NULL, 'Order_placed'),
(26, 62, 'navin kumar', 'n@0.com', '9000000000', 'MI', 'Mi pro', '1561357814.jpg', 'white', 'RS 9900', 'kkkkkkk', '78899', '28Jul', '2019-07-23 05:41:29', 'Office', '727154', '2019-07-23 12:41:29', '2019-07-23 12:41:29', 0, NULL, NULL, 'Order_placed'),
(29, 62, 'navin kumar', 'n@0.com', '9000000000', 'SAMSUNG', 'Galexy s7', '1561357861.jpg', 'black', 'RS 9900', 'kkkkkkk', '78899', '27Jul', '2019-07-23 06:07:26', 'Office', '726486', '2019-07-23 13:07:26', '2019-07-23 13:07:26', 0, NULL, NULL, 'Order_placed'),
(30, 60, 'mkk', 'mk@gmail.com', '1234567899', 'MI', 'Mi pro', '1561357814.jpg', 'white', 'RS 9900', 'undefined', 'undefined', '31Jul', '2019-07-26 10:52:22', 'Home', '476649', '2019-07-26 17:52:22', '2019-07-26 17:52:22', 0, NULL, NULL, 'Order_placed'),
(32, 60, 'mkk', 'mk@gmail.com', '1234567899', 'MI', 'Mi pro', '1561357814.jpg', 'white', 'RS 9900', 'undefined', 'undefined', '31Jul', '2019-07-26 10:52:22', 'Home', '647567', '2019-07-26 17:52:22', '2019-07-26 17:52:22', 0, NULL, NULL, 'Order_placed'),
(34, 69, 'Som Kumar', 'som@backstagesupporters.com', '9911097721', 'SAMSUNG', 'Galexy s7', '1561357861.jpg', 'black', 'RS 9900', 'RZ-D-10, Mahindra Park, Uttam Nagar, Uttam Nagar, Uttam Nagar', 'undefined', '28Jul', '2019-07-26 10:59:25', 'Home', '579287', '2019-07-26 17:59:25', '2019-07-26 17:59:25', 0, NULL, NULL, 'Order_placed'),
(35, 60, 'mkk', 'mk@gmail.com', '1234567899', 'MI', 'Mi pro', '1561357814.jpg', 'white', 'RS 9900', 'errr', 'undefined', '1Aug', '2019-07-27 08:56:57', 'Other', '339390', '2019-07-27 15:56:57', '2019-07-27 15:56:57', 0, NULL, NULL, 'Order_placed');

-- --------------------------------------------------------

--
-- Table structure for table `benifit_about`
--

DROP TABLE IF EXISTS `benifit_about`;
CREATE TABLE IF NOT EXISTS `benifit_about` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benifit_about`
--

INSERT INTO `benifit_about` (`id`, `image`, `title`, `about`, `created_at`, `updated_at`) VALUES
(1, '1560926859.png', 'Benifits', '<p>Helo</p>', '2019-06-19 01:17:39', '2019-06-19 01:17:39');

-- --------------------------------------------------------

--
-- Table structure for table `brand_color`
--

DROP TABLE IF EXISTS `brand_color`;
CREATE TABLE IF NOT EXISTS `brand_color` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `brand_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_model_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brand_color`
--

INSERT INTO `brand_color` (`id`, `brand_id`, `brand_model_id`, `color`, `created_at`, `updated_at`) VALUES
(1, '1', '1', 'Black', '2019-06-23 00:49:26', '2019-06-23 00:49:26'),
(2, '2', '2', 'Grey', '2019-06-23 00:49:45', '2019-06-23 00:49:45'),
(3, '3', '3', 'Black', '2019-06-23 00:49:55', '2019-06-23 00:49:55'),
(4, '4', '4', 'black', '2019-06-24 13:31:41', '2019-06-24 13:31:41'),
(5, '5', '5', 'white', '2019-06-24 13:32:03', '2019-06-24 13:32:03'),
(6, '6', '6', 'black', '2019-06-24 13:32:15', '2019-06-24 13:32:15'),
(7, '4', '4', 'Red', '2019-07-29 17:00:55', '2019-07-29 17:00:55');

-- --------------------------------------------------------

--
-- Table structure for table `brand_details`
--

DROP TABLE IF EXISTS `brand_details`;
CREATE TABLE IF NOT EXISTS `brand_details` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brand_details`
--

INSERT INTO `brand_details` (`id`, `brand_name`, `brand_image`, `status`, `created_at`, `updated_at`) VALUES
(4, 'Google', '1561357680.png', 'Active', '2019-06-24 13:28:00', '2019-06-24 13:28:00'),
(5, 'MI', '1561357695.png', 'Active', '2019-06-24 13:28:15', '2019-06-24 13:28:15'),
(6, 'SAMSUNG', '1561357717.png', 'Active', '2019-06-24 13:28:37', '2019-06-24 13:28:37'),
(7, 'Apple', '1564393893.jpg', 'Active', '2019-07-29 16:51:33', '2019-07-29 16:51:33');

-- --------------------------------------------------------

--
-- Table structure for table `brand_model`
--

DROP TABLE IF EXISTS `brand_model`;
CREATE TABLE IF NOT EXISTS `brand_model` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `brand_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_model_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actualprice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sellingprice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `screenproPrice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brand_model`
--

INSERT INTO `brand_model` (`id`, `brand_id`, `brand_model_name`, `brand_image`, `actualprice`, `sellingprice`, `screenproPrice`, `about`, `created_at`, `updated_at`) VALUES
(4, '4', 'Google Nexus 5', '1561357760.jpg', '500000', '450000', '190000', '<p>Best product</p>', '2019-06-24 13:29:20', '2019-06-24 13:29:20'),
(5, '5', 'Mi pro', '1561357814.jpg', '20000', '20000', '5000', '<p>best mi product</p>', '2019-06-24 13:30:14', '2019-06-24 13:30:14'),
(6, '6', 'Galexy s7', '1561357861.jpg', '25000', '20000', '9000', '<p>best samsung product</p>', '2019-06-24 13:31:01', '2019-06-24 13:31:01');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE IF NOT EXISTS `contact_us` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `phone`, `question`, `message`, `Action`, `created_at`, `updated_at`) VALUES
(1, 'suraj', 'surajkumars177@gmail.com', '9110080863', 'hi', 'hello', 'pending', '2019-06-23 14:23:27', '2019-06-23 14:23:27');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(31, '2014_10_12_000000_create_users_table', 1),
(32, '2014_10_12_100000_create_password_resets_table', 1),
(33, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(34, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(35, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(36, '2016_06_01_000004_create_oauth_clients_table', 1),
(37, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(38, '2019_06_15_062745_create_address', 1),
(39, '2019_06_15_062800_create_slider', 1),
(40, '2019_06_15_062819_create_brand_details', 1),
(41, '2019_06_15_062848_create_brand_model', 1),
(42, '2019_06_15_062917_create_brand_color', 1),
(43, '2019_06_17_053802_create_contact_us', 1),
(44, '2019_06_17_061113_create_about', 1),
(45, '2019_06_17_073324_create__state_available', 1),
(46, '2019_06_18_121119_create_termsandcondition', 2),
(47, '2019_06_18_121214_create_benifit_about', 2),
(48, '2019_06_20_072536_create_appoinment', 3),
(49, '2019_06_20_091331_create_questionanswer', 4);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questionanswer`
--

DROP TABLE IF EXISTS `questionanswer`;
CREATE TABLE IF NOT EXISTS `questionanswer` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questionanswer`
--

INSERT INTO `questionanswer` (`id`, `title`, `about`, `created_at`, `updated_at`) VALUES
(1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2019-07-31 12:36:28', '2019-07-31 12:36:28');

-- --------------------------------------------------------

--
-- Table structure for table `selectcity`
--

DROP TABLE IF EXISTS `selectcity`;
CREATE TABLE IF NOT EXISTS `selectcity` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `select_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brow_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `image`, `created_at`, `updated_at`) VALUES
(2, '1564576407.png', '2019-07-31 19:33:27', '2019-07-31 19:33:27');

-- --------------------------------------------------------

--
-- Table structure for table `termsandcondition`
--

DROP TABLE IF EXISTS `termsandcondition`;
CREATE TABLE IF NOT EXISTS `termsandcondition` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `track_order`
--

DROP TABLE IF EXISTS `track_order`;
CREATE TABLE IF NOT EXISTS `track_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `placed_order` int(11) DEFAULT NULL,
  `order_confirmed` int(11) DEFAULT NULL,
  `agent_assign` int(11) DEFAULT NULL,
  `order_complete` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `track_order`
--

INSERT INTO `track_order` (`id`, `order_id`, `user_id`, `placed_order`, `order_confirmed`, `agent_assign`, `order_complete`, `created_at`, `updated_at`) VALUES
(1, 891295, 1, 1, NULL, NULL, NULL, '2019-06-25 16:48:07', '2019-06-25 16:48:07'),
(2, 808358, 1, 1, NULL, NULL, NULL, '2019-06-25 23:22:45', '2019-06-25 23:22:45'),
(3, 41530540, 1, 1, NULL, NULL, NULL, '2019-06-25 23:42:06', '2019-06-25 23:42:06'),
(4, 811719, 1, 1, NULL, NULL, NULL, '2019-06-26 01:44:45', '2019-06-26 01:44:45'),
(5, 942485, 2, 1, NULL, NULL, NULL, '2019-06-26 02:03:53', '2019-06-26 02:03:53'),
(6, 257325, 1, 1, NULL, NULL, NULL, '2019-06-26 02:55:03', '2019-06-26 02:55:03'),
(7, 251181, 2, 1, NULL, NULL, NULL, '2019-06-26 08:33:00', '2019-06-26 08:33:00'),
(8, 653714, 2, 1, NULL, NULL, NULL, '2019-06-26 08:49:25', '2019-06-26 08:49:25'),
(9, 161552, 2, 1, NULL, NULL, NULL, '2019-06-26 08:49:52', '2019-06-26 08:49:52'),
(10, 290533, 2, 1, NULL, NULL, NULL, '2019-06-26 08:50:46', '2019-06-26 08:50:46'),
(11, 439896, 2, 1, NULL, NULL, NULL, '2019-06-26 09:13:18', '2019-06-26 09:13:18'),
(12, 694692, 2, 1, NULL, NULL, NULL, '2019-06-26 11:59:48', '2019-06-26 11:59:48'),
(13, 795682, 2, 1, NULL, NULL, NULL, '2019-06-26 13:18:27', '2019-06-26 13:18:27'),
(14, 754021, 2, 1, NULL, NULL, NULL, '2019-06-26 13:34:52', '2019-06-26 13:34:52'),
(15, 124958, 2, 1, NULL, NULL, NULL, '2019-06-27 01:43:26', '2019-06-27 01:43:26'),
(16, 331034, 2, 1, NULL, NULL, NULL, '2019-06-27 12:31:21', '2019-06-27 12:31:21'),
(17, 676448, 2, 1, NULL, NULL, NULL, '2019-06-27 12:58:27', '2019-06-27 12:58:27'),
(18, 255201, 2, 1, NULL, NULL, NULL, '2019-06-27 13:39:30', '2019-06-27 13:39:30'),
(19, 855242, 2, 1, NULL, NULL, NULL, '2019-06-27 23:42:01', '2019-06-27 23:42:01'),
(20, 640976, 2, 1, NULL, NULL, NULL, '2019-07-01 05:31:10', '2019-07-01 05:31:10'),
(21, 523658, 2, 1, NULL, NULL, NULL, '2019-07-01 05:34:08', '2019-07-01 05:34:08'),
(22, 279299, 2, 1, NULL, NULL, NULL, '2019-07-01 05:35:34', '2019-07-01 05:35:34'),
(23, 998746, 38, 1, NULL, NULL, NULL, '2019-07-19 17:26:43', '2019-07-19 17:26:43'),
(24, 681186, 41, 1, NULL, NULL, NULL, '2019-07-19 20:17:16', '2019-07-19 20:17:16'),
(25, 230062, 43, 1, NULL, NULL, NULL, '2019-07-20 04:16:02', '2019-07-20 04:16:02'),
(26, 541049, 43, 1, NULL, NULL, NULL, '2019-07-20 04:42:43', '2019-07-20 04:42:43'),
(27, 847709, 35, 1, NULL, NULL, NULL, '2019-07-20 05:24:58', '2019-07-20 05:24:58'),
(28, 198555, 35, 1, NULL, NULL, NULL, '2019-07-20 05:39:29', '2019-07-20 05:39:29'),
(29, 534855, 44, 1, NULL, NULL, NULL, '2019-07-20 12:02:02', '2019-07-20 12:02:02'),
(30, 866637, 46, 1, NULL, NULL, NULL, '2019-07-20 12:53:12', '2019-07-20 12:53:12'),
(31, 177083, 47, 1, NULL, NULL, NULL, '2019-07-20 13:56:27', '2019-07-20 13:56:27'),
(32, 727154, 62, 1, NULL, NULL, NULL, '2019-07-23 12:41:29', '2019-07-23 12:41:29'),
(33, 479591, 62, 1, NULL, NULL, NULL, '2019-07-23 12:58:31', '2019-07-23 12:58:31'),
(34, 329122, 62, 1, NULL, NULL, NULL, '2019-07-23 13:05:29', '2019-07-23 13:05:29'),
(35, 726486, 62, 1, NULL, NULL, NULL, '2019-07-23 13:07:26', '2019-07-23 13:07:26'),
(36, 476649, 60, 1, NULL, NULL, NULL, '2019-07-26 17:52:22', '2019-07-26 17:52:22'),
(37, 995622, 60, 1, NULL, NULL, NULL, '2019-07-26 17:52:22', '2019-07-26 17:52:22'),
(38, 647567, 60, 1, NULL, NULL, NULL, '2019-07-26 17:52:22', '2019-07-26 17:52:22'),
(39, 734369, 60, 1, NULL, NULL, NULL, '2019-07-26 17:52:22', '2019-07-26 17:52:22'),
(40, 579287, 69, 1, NULL, NULL, NULL, '2019-07-26 17:59:25', '2019-07-26 17:59:25'),
(41, 339390, 60, 1, NULL, NULL, NULL, '2019-07-27 15:56:57', '2019-07-27 15:56:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `verified` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `email_verified_at`, `password`, `otp`, `otp_at`, `remember_token`, `created_at`, `updated_at`, `verified`) VALUES
(1, 'suraj', 'admin@gmail.com', '9110080863', NULL, '$2y$10$Ra5JrHPZjWh7hT1IrwerTue10u848x4Dywc1TNPB391Axv4PL1I7y', '228650', '2019-06-27 06:06:22', NULL, '2019-06-25 13:24:24', '2019-06-25 13:24:24', 1),
(2, 'vikas', 'v8.vikas@gmail.com', '8959893980', NULL, '$2y$10$OQ5PgdlAyBU6LVVRbdhP8uGURk02TvH7Is0CkqXmUd1jKlBM6dMk6', '123456', NULL, NULL, '2019-06-25 21:21:15', '2019-06-25 21:21:15', 1),
(3, NULL, NULL, '7696707842', NULL, NULL, '123456', NULL, NULL, '2019-06-26 11:51:01', '2019-06-26 11:51:01', 0),
(4, NULL, NULL, '9560755823', NULL, NULL, '123456', NULL, NULL, '2019-06-26 22:11:55', '2019-06-26 22:11:55', 0),
(5, NULL, NULL, '8377955675', NULL, NULL, '123456', NULL, NULL, '2019-06-26 22:19:48', '2019-06-26 22:19:48', 0),
(6, NULL, NULL, '000000000', NULL, NULL, '123456', NULL, NULL, '2019-06-26 22:32:40', '2019-06-26 22:32:40', 0),
(7, NULL, NULL, '8470095700', NULL, NULL, '123456', NULL, NULL, '2019-06-26 22:35:48', '2019-06-26 22:35:48', 0),
(8, NULL, NULL, '567876543456', NULL, NULL, '123456', NULL, NULL, '2019-06-27 11:47:16', '2019-06-27 11:47:16', 0),
(9, NULL, NULL, '7888535058', NULL, NULL, '123456', NULL, NULL, '2019-06-27 23:55:32', '2019-06-27 23:55:32', 0),
(10, NULL, NULL, '89898988', NULL, NULL, '123456', NULL, NULL, '2019-06-28 11:09:14', '2019-06-28 11:09:14', 0),
(11, NULL, NULL, '01234567898', NULL, NULL, '123456', NULL, NULL, '2019-06-28 21:21:36', '2019-06-28 21:21:36', 0),
(12, NULL, NULL, '08377955675', NULL, NULL, '123456', NULL, NULL, '2019-06-29 20:00:45', '2019-06-29 20:00:45', 0),
(13, NULL, NULL, '747447744545', NULL, NULL, '123456', NULL, NULL, '2019-07-01 19:45:29', '2019-07-01 19:45:29', 0),
(14, NULL, NULL, '9560674031', NULL, NULL, '123456', NULL, NULL, '2019-07-01 20:07:00', '2019-07-01 20:07:00', 0),
(15, NULL, NULL, '8768768678678', NULL, NULL, '123456', NULL, NULL, '2019-07-03 17:55:05', '2019-07-03 17:55:05', 0),
(16, NULL, NULL, '7835987554', NULL, NULL, '123456', NULL, NULL, '2019-07-03 23:32:04', '2019-07-03 23:32:04', 0),
(17, NULL, NULL, '0000000000', NULL, NULL, '123456', NULL, NULL, '2019-07-07 13:43:29', '2019-07-07 13:43:29', 0),
(18, NULL, NULL, '0000000000', NULL, NULL, '123456', NULL, NULL, '2019-07-07 13:43:29', '2019-07-07 13:43:29', 0),
(19, 'navin', 'navin123@gmail.com', '7878787878', NULL, '$2y$10$2T8ExKM.BM2Y2WQOP3bgcezksiUXi7BlXJ6aqR/SKRBKdx0ojw8Sy', '123456', NULL, NULL, '2019-07-08 12:44:56', '2019-07-08 12:44:56', 1),
(20, NULL, NULL, '6767676767', NULL, NULL, '123456', NULL, NULL, '2019-07-08 15:01:35', '2019-07-08 15:01:35', 0),
(21, NULL, NULL, '1234567890', NULL, NULL, '123456', NULL, NULL, '2019-07-08 15:10:55', '2019-07-08 15:10:55', 0),
(22, NULL, NULL, '76788946785', NULL, NULL, '123456', NULL, NULL, '2019-07-08 15:16:26', '2019-07-08 15:16:26', 0),
(23, NULL, NULL, '76788946785', NULL, NULL, '123456', NULL, NULL, '2019-07-08 15:16:26', '2019-07-08 15:16:26', 0),
(24, NULL, NULL, '76788946785', NULL, NULL, '123456', NULL, NULL, '2019-07-08 15:16:26', '2019-07-08 15:16:26', 0),
(25, NULL, NULL, '76788946785', NULL, NULL, '123456', NULL, NULL, '2019-07-08 15:16:26', '2019-07-08 15:16:26', 0),
(26, NULL, NULL, '9595959595', NULL, NULL, '123456', NULL, NULL, '2019-07-08 15:58:22', '2019-07-08 15:58:22', 0),
(27, NULL, NULL, '202020202020', NULL, NULL, '123456', NULL, NULL, '2019-07-08 16:11:54', '2019-07-08 16:11:54', 0),
(28, 'kl', 'kl@gmail.com', '7696707868', NULL, '$2y$10$s7beYGQmRqhIqpuy.JrNd.t8X4dl/tURyWoGQO4Qp7ltOjboWxFxK', '123456', NULL, NULL, '2019-07-08 19:19:00', '2019-07-08 19:19:00', 1),
(29, 'kkkk', 'kkkk@gmail.com', '7696707800', NULL, '$2y$10$8FXk8n8fW08jBtI/lwtNaONrPJcqUR/pAWqQSg1pMk7lfr4FRnL5a', '123456', NULL, NULL, '2019-07-09 14:41:33', '2019-07-09 14:41:33', 1),
(30, NULL, NULL, '9999999999', NULL, NULL, '123456', NULL, NULL, '2019-07-09 15:52:32', '2019-07-09 15:52:32', 0),
(31, NULL, NULL, '2525252525', NULL, NULL, '123456', NULL, NULL, '2019-07-09 16:04:57', '2019-07-09 16:04:57', 0),
(32, 'klk', 'kl78@gmail.com', '2525252526', NULL, '$2y$10$cVhQ4jQpUXwjPOlGNZEMbeM4B//j5w4ngVQrXOG0sBHXbNA9B3Eju', '123456', NULL, NULL, '2019-07-09 16:06:00', '2019-07-09 16:06:00', 1),
(33, NULL, NULL, '7894561230', NULL, NULL, '123456', NULL, NULL, '2019-07-10 19:05:51', '2019-07-10 19:05:51', 0),
(34, 'wer', 'nkk@gmail.com', '7894561233', NULL, '$2y$10$WquHvEpbKAQDviHIpGp0pOqogwI8jIi9PplWhZ22vmfi0UKJYm8/K', '123456', NULL, NULL, '2019-07-10 19:15:52', '2019-07-10 19:15:52', 1),
(35, 'vk', 'vk@gmail.com', '2222222222', NULL, '$2y$10$jaVvgHqi73umS/pk5ptj4O9MSqP.Bfluv2sb.Krbiz5N16V14uE0a', '123456', NULL, NULL, '2019-07-17 13:58:01', '2019-07-17 13:58:01', 1),
(36, NULL, NULL, '9922367414', NULL, NULL, '123456', NULL, NULL, '2019-07-19 09:08:52', '2019-07-19 09:08:52', 0),
(37, NULL, NULL, '777777777777777777', NULL, NULL, '123456', NULL, NULL, '2019-07-19 17:16:28', '2019-07-19 17:16:28', 0),
(38, 'nnnn', 'nnnn@gmail.com', '1212121212', NULL, '$2y$10$7rpGMQTh5ns8mPcwlo/4SuC3Gfxy6vWiu32jRZwh3fOL0Z5X1Ab7i', '123456', NULL, NULL, '2019-07-19 17:19:48', '2019-07-19 17:19:48', 1),
(39, 'rk', 'rk@gmail.com', '3333333333', NULL, '$2y$10$Rh6BO5rXHSXrKxxX9NxP4ej5iZonRz0dO0hRdaFoa7ctiSA1Po97K', '123456', NULL, NULL, '2019-07-19 18:48:51', '2019-07-19 18:48:51', 1),
(40, NULL, NULL, '8528528520', NULL, NULL, '123456', NULL, NULL, '2019-07-19 20:12:08', '2019-07-19 20:12:08', 0),
(41, 'sdfghjkl', 's@gmail.com', '8528528521', NULL, '$2y$10$yQq9WPNTiV4X0GMIh.NsN.kHDGA1LjRRC9RT6jTqV33s5Hn0K8w0q', '123456', NULL, NULL, '2019-07-19 20:14:53', '2019-07-19 20:14:53', 1),
(42, 'nkk', 'b@gmail.com', '111111111111', NULL, '$2y$10$Jw.hzxG1M1fUCfJW7jYlzem7.PsniJPgllrZu0LMHR/4fbmQFqBJ.', '123456', NULL, NULL, '2019-07-20 03:08:08', '2019-07-20 03:08:08', 1),
(43, 'nnnkkk', 'v@gmail.com', '5555555555', NULL, '$2y$10$Pa/Z2TkiTCWM6rYWv1pyK.i92LC7a9M2WhaOjoNoJ8apInwEEdiOK', '123456', NULL, NULL, '2019-07-20 04:14:34', '2019-07-20 04:14:34', 1),
(44, 'lll', 'l@gmail.com', '22222222222', NULL, '$2y$10$M7Hlrs/iK3q/9XLkButaquH09T1gJZbZjhLXNSc8xTR.5WDD3gTuq', '123456', NULL, NULL, '2019-07-20 11:40:29', '2019-07-20 11:40:29', 1),
(45, NULL, NULL, '1234567891', NULL, NULL, '123456', NULL, NULL, '2019-07-20 12:25:51', '2019-07-20 12:25:51', 0),
(46, 'Kkkk', 'Kkk@gmail.com', '9090909090', NULL, '$2y$10$FF7k1WcfPG9S8MUJ8EDIM.bFDiB8zb92wVFCG1RRStiJWSNHE0hQq', '123456', NULL, NULL, '2019-07-20 12:50:04', '2019-07-20 12:50:04', 1),
(47, 'Not in bhai', 'N@gmail.com', '999999998', NULL, '$2y$10$WgKhj2WF.vEMLR83aPO28OT0.7x25VttjMRrYoX4nFT6HMp6US4PO', '123456', NULL, NULL, '2019-07-20 13:55:26', '2019-07-20 13:55:26', 1),
(48, NULL, NULL, '9560684031', NULL, NULL, '123456', NULL, NULL, '2019-07-20 16:16:20', '2019-07-20 16:16:20', 0),
(49, 'Vinit Gupta', 'Vin@gmai.com', '847095700', NULL, '$2y$10$LMkKzXDunHB4/YMAz17sVuGRJ.VKOhQqc8IFw9k6M0FxeJEtATI52', '123456', NULL, NULL, '2019-07-20 18:43:09', '2019-07-20 18:43:09', 1),
(50, 'Kkkkkkk', 'k@gmail.com', '999999999', NULL, '$2y$10$NiGcNeuLe9NUYBX0QhrN.eDLQEqs1wNCQ7IXuZhAFYiT/oEN6Zq46', '123456', NULL, NULL, '2019-07-20 18:55:04', '2019-07-20 18:55:04', 1),
(51, NULL, NULL, '7777777777', NULL, NULL, '123456', NULL, NULL, '2019-07-20 21:13:16', '2019-07-20 21:13:16', 0),
(52, NULL, NULL, '7777777779', NULL, NULL, '123456', NULL, NULL, '2019-07-20 21:14:43', '2019-07-20 21:14:43', 0),
(53, 'Seven', 'sev@gmail.com', '0987654321', NULL, '$2y$10$VUtpyYkF6vWtt5zOyVxYYu928NP758WDSKDW4y7Ojgb8nGhhLu4XC', '123456', NULL, NULL, '2019-07-20 21:15:28', '2019-07-20 21:15:28', 1),
(54, NULL, NULL, '00000000000', NULL, NULL, '123456', NULL, NULL, '2019-07-20 21:22:36', '2019-07-20 21:22:36', 0),
(55, NULL, NULL, '1010101010', NULL, NULL, '123456', NULL, NULL, '2019-07-20 21:23:22', '2019-07-20 21:23:22', 0),
(56, 'Nnnnn', 'nk@yahu.com', '1010101011', NULL, '$2y$10$v5MzUApCt8DiHoQsJ5uz6uMW.5Oqq7hstU64B9W28KzhRkk3bgDL.', '123456', NULL, NULL, '2019-07-20 21:23:49', '2019-07-20 21:23:49', 1),
(57, NULL, NULL, '3838383838', NULL, NULL, '123456', NULL, NULL, '2019-07-20 22:48:04', '2019-07-20 22:48:04', 0),
(58, 'xcvbnm,.,mnbvccvbnm,', 'sp@g.com', '5555555555555', NULL, '$2y$10$zpjWlyXH28S/Ibs1w5MtOeebxt9CjSjzURDZ0p5wkIZgm9NlxM962', '123456', NULL, NULL, '2019-07-20 22:53:58', '2019-07-20 22:53:58', 1),
(59, 'hhh', 'h@.com', '456456', NULL, '$2y$10$QerZPh5sIyV6xoup5MJmpufq0Qy8gE/OTr2184e93vawKHC8/xCrO', '123456', NULL, NULL, '2019-07-20 23:06:01', '2019-07-20 23:06:01', 1),
(60, 'mkk', 'mk@gmail.com', '1234567899', NULL, '$2y$10$TwuevMwEphu/Vsh2F6oLDew1VTIB4Y063fO1zi1Ba2qgCvHv/ABSe', '123456', NULL, NULL, '2019-07-23 04:43:08', '2019-07-23 04:43:08', 1),
(61, NULL, NULL, '8888888888', NULL, NULL, '123456', NULL, NULL, '2019-07-23 05:53:00', '2019-07-23 05:53:00', 0),
(62, 'navin kumar', 'n@0.com', '9000000000', NULL, '$2y$10$JkgqzkZ2UUk9.fj8HCVm0ujbFy8Z3fvffKzs6CI/sshTQb5auCqb6', '123456', NULL, NULL, '2019-07-23 12:30:24', '2019-07-23 12:30:24', 1),
(63, 'Vinit Gupta', 'G@g.com', '7835986554', NULL, '$2y$10$kHFlsSoNL11eA.ZKYrpqtumP3yhkrJs7woHPcZiFWT0y95W9U6bHG', '123456', NULL, NULL, '2019-07-23 12:42:31', '2019-07-23 12:42:31', 1),
(64, 'dfghjkl', 'sdfghuijhgfdx@gmail.com', '90000000000', NULL, '$2y$10$NPJHS1mkruZin4CqK76YueKeEqvMc1P/BRSNkiIbc258MWE43lyAS', '123456', NULL, NULL, '2019-07-23 12:54:42', '2019-07-23 12:54:42', 1),
(65, 'arvind', 'vishalgupta285@gmail.com', '8000000000', NULL, '$2y$10$aMSDWLAM43Qv.A61uSuUpOyyWxd3/85wngMDmZIgO1zLkymJdr0XO', '123456', NULL, NULL, '2019-07-23 13:00:45', '2019-07-23 13:00:45', 1),
(66, 'Yes', 'Yy.6@g.com', '888888888', NULL, '$2y$10$CCTxHHom0v1hSvRVBUp4Vuthie.KukOB/qBYfhF4VSyPXAThaOwWq', '123456', NULL, NULL, '2019-07-23 22:58:19', '2019-07-23 22:58:19', 1),
(67, NULL, NULL, '88888888', NULL, NULL, '123456', NULL, NULL, '2019-07-26 01:27:23', '2019-07-26 01:27:23', 0),
(68, NULL, NULL, '8888888889', NULL, NULL, '123456', NULL, NULL, '2019-07-26 01:30:15', '2019-07-26 01:30:15', 0),
(69, 'Som Kumar', 'som@backstagesupporters.com', '9911097721', NULL, '$2y$10$i2Lcg.q0dkdLHdFS/AFG2u.ylIIPufmxGq17/IBXLH/anf85vHvhu', '123456', NULL, NULL, '2019-07-26 17:57:50', '2019-07-26 17:57:50', 1),
(70, 'hk', 'somphpdeveloper@gmail.com', '786888909', NULL, '$2y$10$The7wGEJBqtFKfIYbgub4.ft.7G23AiQSKHnBIyMDrJChT8EvtcdG', '123456', NULL, NULL, '2019-07-26 18:03:14', '2019-07-26 18:03:14', 1),
(71, 'jyotika sethi', 'jyotika@backstagesupporters.com', '9056560413', NULL, '$2y$10$qRZ/W3UVDRPDuNM13hqUNOpUOC.lBxSyeyvplMqwcZIVjhwc6pRiy', '123456', NULL, NULL, '2019-07-27 17:16:43', '2019-07-27 17:16:43', 1),
(72, NULL, NULL, '9953891928', NULL, '$2y$10$6FTTB9HQhZa.J5PAv8V3Iuu/mbTB9EIkJGHLXjskw4rJoJlVDBorG', '123456', NULL, NULL, '2019-07-29 12:53:50', '2019-07-29 12:53:50', 0);

-- --------------------------------------------------------

--
-- Table structure for table `_state_available`
--

DROP TABLE IF EXISTS `_state_available`;
CREATE TABLE IF NOT EXISTS `_state_available` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Available',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `_state_available`
--

INSERT INTO `_state_available` (`id`, `state`, `pin`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Bihar', '825301', '1560930116.png', 'Available', '2019-06-19 02:11:56', '2019-06-19 02:11:56'),
(2, 'Punjab', '152004', '1561224995.png', 'Available', '2019-06-23 00:36:35', '2019-06-23 00:36:35'),
(3, 'Chandigarh', '160004', '1561225043.png', 'Available', '2019-06-23 00:37:23', '2019-06-23 00:37:23');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
