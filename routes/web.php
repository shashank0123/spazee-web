<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('AdminPanel.Dashboard');
});



Route::get('generate-pdf', 'AdminController@pdfview')->name('generate-pdf');





Route::get('/ajax_upload', 'UserController@index');

Route::post('/ajax_upload/action', 'UserController@action')->name('ajaxupload.action');


Route::get('myform/ajax/{id}',array('as'=>'myform.ajax','uses'=>'AdminController@myformAjax'));


Route::post('/admin/slider', 'AdminController@AddImage');

Route::get('/admin/slider-image','AdminController@showSlider');
Route::post('/admin/slider-image-edit','AdminController@EditSlider');
Route::get('/admin/slider-image/delete/{id}',function($id){

	$delete=DB::delete('delete  from slider where id=?',[$id]);
	return Redirect::back()->with('success','successfully delete');
	
});

Route::post('/admin/brandname','AdminController@AddBrand');
Route::get('/showbrandDetails','AdminEditController@showbrandDetails');
Route::post('/admin/brand_name-edit','AdminEditController@editbrandDetails');
Route::get('/admin/brand_details/{id}',function($id){

	$delete=DB::delete('delete  from brand_details where id=?',[$id]);
	return Redirect::back()->with('success','successfully delete');
});

Route::view('/questionanswer','AdminPanel/questionanswer');
Route::post('/admin/questionAnswer','AdminController@questionAnswer');
Route::get('/showF&Q','AdminController@showqa');

Route::post('/admin/brandmodel','AdminController@brandModel');
Route::get('/showModel','AdminEditController@showModel');
Route::post('/admin/edit-model-page','AdminEditController@editbrandModel');
Route::get('/admin/brand_edit-model/{id}','AdminEditController@EditModelPage');
Route::get('/admin/brand_model/{id}', function($id){
$delete=DB::delete('delete  from brand_model where id=?',[$id]);
	return Redirect::back()->with('success','successfully delete');

});

Route::get('/terms-condition', 'AdminController@getTerms');
Route::post('/admin/termsCondition','AdminController@terms');
Route::get('/admin/terms','AdminController@showterms');
Route::post('/admin/edit-termsCondition','AdminEditController@editTerms');

Route::view('/benifitsterms','AdminPanel/benifitabout');
Route::post('/admin/benifitsterms','AdminController@benifitabout');
Route::get('/benifit-view', 'AdminController@benifitshow');
Route::get('/admin/benifit/edit/{id}', 'AdminController@shwbenifitshow');

Route::post('/admin/AvailableStatus','AdminController@Available');
Route::get('/admin/show/AvailableStatus','AdminController@showavailable');

Route::post('/admin/brandcolor','AdminController@brandcolor');
Route::get('/brandcolor-page','AdminEditController@brandcolor');

Route::post('/admin/About', 'AdminController@About12');

Route::view('/admin','AdminPanel/Dashboard');

Route::get('/about','AdminController@about');
Route::get('/admin/about-edit','AdminController@showAbout');
Route::post('/admin/About-edit/{id}','AdminController@editAbout');

Route::get('/admin-home','AdminController@home');
Route::get('/contact','AdminController@contact');

Route::post('/admin/AvailableStatus','AdminController@AvailableStatus');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin/users-list', 'AdminController@getAllUsers');

Route::get('/admin/orders', 'AdminController@getAllOrders');

Route::get('/admin/invoice/{id}', 'AdminController@getInvoice');

Route::get('/admin/edit-user-detail/{id}', 'AdminController@editUser');

Route::post('/admin/change-status', 'AdminController@changeOrderStatus');

Route::get('/admin/price', 'AdminController@getPrice');

Route::post('/admin/users-list/{id}',function($id){

	$delete=DB::delete('delete  from users where id=?',[$id]);
	return Redirect::back()->with('success','successfully delete');
});