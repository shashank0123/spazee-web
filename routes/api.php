<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('register', 'AuthApiController@register');
Route::post('validate_otp', 'AuthApiController@login');
Route::group(['middleware' => ['jwt.auth']], function() { 
  Route::get('logout', 'AuthApiController@logout');
  Route::get('/userdetails', 'UserController@userdetails');
  Route::get('/orders', 'UserOrderController@order_list');
  Route::get('/orders/{id}', 'UserOrderController@order_details');
  
});

Route::post('/useraddress','UserController@UserAddress');
Route::post('/updateaddress','UserController@updateaddress');
Route::post('/deleteaddress','UserController@deleteaddress');
Route::post('/user-register', 'UserController@register12');
Route::post('/user-verifyotp', 'UserController@verifyOtp');
Route::post('/resendOtp', 'UserController@resendOtp');
Route::get('/searchCity','AdminApiController@searchCity');
Route::get('/termsandcondition','AdminApiController@termsandcondition');
Route::get('/AvailableStatus','AdminApiController@AvailableStatus');
Route::get('/previouscity','AdminApiController@previouscity');
Route::get('/brands','AdminApiController@brands');
Route::get('/cityList','AdminApiController@cityList');
Route::get('/BrandDetails','AdminApiController@BrandDetails');

Route::get('/barandsmodel','AdminApiController@barandsmodel');
Route::get('/brandColor','AdminApiController@brandColor');
Route::get('/productList','AdminApiController@productList');
Route::post('/contactUs','UserController@contactUs');

Route::post('/selectcity/{token}','UserController@selectcity');
Route::post('/sendotp', 'UserController@sendotp');
Route::get('/brandModel', 'AdminApiController@brandModel');
Route::get('/review_list', 'ReviewController@review_list');
Route::get('/available_dates', 'AvailabilityApiController@available_dates');
Route::get('/estimated_price', 'ItemPriceController@estimated_price');
Route::get('/get-brandname','AdminApiController@brandName');

 Route::post('/profileupdate','UserController@profileupdate');
Route::post('/login', 'AuthController@login');
Route::post('/rescedule','AdminApiController@rescedule');
Route::get('/locationstatus','UserController@locationstatus');
Route::post('/deleteappoinment','AdminApiController@deleteAppoinment');
Route::post('/verifymobile','AdminApiController@verifymobile');
Route::get('/updateappoinmentno12','AdminApiController@UpdateAppoinmentNo');
Route::get('/orderdetails','AdminApiController@orderDetails');

Route::group([

    'middleware' => 'api',
    
], function () {
    
    Route::post('/logout', 'AuthController@logout');
    Route::post('/refresh', 'AuthController@refresh');
    Route::post('/me', 'AuthController@me');
    Route::get('/authdata', 'UserController@authdata');
    Route::get('/userdetails','UserController@userdetails');
	Route::get('/brandmodel','AdminApiController@BrandModel');
	Route::get('/brandcolor','AdminApiController@brandcolor');
	Route::get('/brandaddress/{id}','AdminApiController@BrandAddress');
   	Route::get('/benifitabout','AdminApiController@benifitabout');
   	Route::get('/aboutUs','AdminApiController@AboutUs');
   	
    Route::post('/fromrequest','AdminApiController@fromRequest');
   	Route::get('/OrderHistory','AdminApiController@OrderHistory');
    Route::post('/appoinment', 'AdminApiController@appoinment');
   	Route::get('/servicedetails/{id}','AdminApiController@servicedetails');
   	Route::get('/fq', 'AdminApiController@F_Q');
   	Route::get('/ordertrack','AdminApiController@ordertrack');
   


});
