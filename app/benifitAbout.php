<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class benifitAbout extends Model
{
    protected $table='benifit_about';

    protected $fillable=['image','title','about']; 
}
