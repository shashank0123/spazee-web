<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\slider;
use DB;
use PDF;
use Redirect;
use App\brandDetails;
use App\User;
use App\brandModel;
use App\brandcolor;
use App\appoinment;
use App\contact;
use App\AboutUs;
use App\AvailableStatus;
use App\termsandcondition;
use App\benifitabout;
use App\questionanswer;


class AdminController extends Controller
{

  public function home()
  {
   return view('AdminPanel/Home');
 }

 public function AddImage(Request $request){

  request()->validate([

            // 'images' =>'dimensions:width=100,height=100',
    'images' =>'required|mimes:jpeg,png,jpg,gif,svg|max:5000',

  ], [


  ]);

  $image = $request->file('images');
  $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
  $destinationPath = public_path('/images');
  $image->move($destinationPath, $input['imagename']);

  $slider=$input['imagename'];

  $data = array('image' =>$slider, 'created_at'=>NOW(), 'updated_at' =>NOW());

  $save = DB::table('slider')->insert($data);

  if($save==true){

    return Redirect::back()->with('success', 'Sucessfully insert slider');
  }else{
    return Redirect::back()->with('danger', 'check error');

  }
}

public function showSlider(){

  $data=slider::all();
  return view('AdminPanel/EditSlider',['data'=>$data]);
}

public function EditSlider( Request $request){

 request()->validate([

  'images' =>'required|mimes:jpeg,png,jpg,gif,svg|max:5000',

], [


]);
 $id=request('id');
 $image = $request->file('images');
 $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
 $destinationPath = public_path('/images');
 $image->move($destinationPath, $input['imagename']);

 $slider=$input['imagename'];


 $save = DB::update('update slider set image=? where id=?',[$slider, $id]);

 if($save==true){

  return Redirect::back()->with('success', 'Sucessfully Update slider Image');
}else{
  return Redirect::back()->with('danger', 'check error');

}


}

public function  AddBrand(Request $request){

  request()->validate([

    'BrandLogo' =>'required|mimes:jpeg,png,jpg,gif,svg|max:5000',

  ],[



  ]);

  $image = $request->file('BrandLogo');
  $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
  $destinationPath = public_path('/images');
  $image->move($destinationPath, $input['imagename']);
  $brandLogo=$input['imagename'];
  $data = new brandDetails;
  $data->brand_name=request('BrandName');
  $data->brand_image=$brandLogo;

  $submit=$data->save();

  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully submit the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}

public function brandModel(Request $request){

  request()->validate([
    'BrandModelImage'=>'required|mimes:jpeg,png,jpg,gif,svg|max:5000',
  ],[

  ]);

  $image = $request->file('BrandModelImage');
  $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
  $destinationPath = public_path('/images');
  $image->move($destinationPath, $input['imagename']);
  $brand_image=$input['imagename'];

  $data= new brandModel;
  $data->brand_id=request('brand_id');
  $data->brand_model_name=request('BrandModel');
  $data->brand_image=$brand_image;
  $data->actualprice=request('actualprice');
  $data->sellingprice=request('SellingPrice');
  $data->screenproPrice=request('ScreenproPrice');
  $data->about=request('about');
  $submit=$data->save();

  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully submit the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}

public function brandcolor(Request $request){

  request()->validate([

  ],[


  ]);

  $data= new brandcolor;

  $data->brand_id=request('brand_name');
  $data->brand_model_id=request('brand_model');
  $data->color=request('color');

  $submit=$data->save();

  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully submit the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}
public function myformAjax($id)
{

  $data=brandModel::all()->where('brand_id','=',$id);

  return json_encode($data);
}



public function contact()
{
 $data=contact::all();
 return view('/AdminPanel/ContactUs',['data'=>$data]);
}

public function about()
{
  $about = AboutUs::orderBy('created_at','DESC')->limit(1)->first();
  return view('/AdminPanel/AboutUs',compact('about'));   
}
public function About12(Request $request)

{
 request()->validate([
 ],[

 ]);

 $data= new AboutUs;
 $data->about=request('about');
 $submit=$data->save();

 if($submit==true){

  return Redirect::back()->with('success', 'Sucessfully submit the data');
}else{
  return Redirect::back()->with('danger','error please check');
}


}

public function showAbout(){

  $show=AboutUs::all()->where('id','=',1);
  return view('AdminPanel/EditAbout',['show'=>$show]);
}

public function editAbout($id){

  $update=AboutUs::find($id);

  $update->about=request('about');
  $submit=$update->save();
  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully update the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}


public function AvailableStatus(Request $request){

  request()->validate([
  ],[
    'image'=>'required|mimes:jpeg,png,jpg,gif,svg|max:5000',

  ]);

  $image = $request->file('image');
  $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
  $destinationPath = public_path('/images');
  $image->move($destinationPath, $input['imagename']);
  $brandLogo=$input['imagename'];    
  $data= new AvailableStatus;

  $data->state=request('state');
  $data->pin=request('pincode');
  $data->image=$brandLogo;
  $data->status=request('status');
  $submit=$data->save();
  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully submit the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}

public function terms(Request $request){

 request()->validate([
 ],[


 ]);

 $data=new termsandcondition;
 $data->about=request('about');
 $submit=$data->save();
 if($submit==true){

  return Redirect::back()->with('success', 'Sucessfully submit the data');
}else{
  return Redirect::back()->with('danger','error please check');
}
}

public function getTerms(){
  $show = termsandcondition::orderBy('created_at','DESC')->limit(1)->first();
  return view('AdminPanel.termsCondition',compact('show'));
}
public function showterms(){

 $show = termsandcondition::orderBy('created_at','DESC')->limit(1)->first();

 return View('AdminPanel/editTerms',['show'=>$show]);
}

public function editTerms($id){

  $update=termsandcondition::find($id);

  $update->about=request('about');
  $submit=$update->save();
  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully update the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}

public function benifitabout(Request $request){
  $id = $request->id;

  request()->validate([

  ],[


  ]);

  if(!empty($id)){
    $image = $request->file('logo');
    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
    $destinationPath = public_path('/images');
    $image->move($destinationPath, $input['imagename']);
    $brand_image=$input['imagename'];

    $data = benifitabout::find($id);
    $data->image=$brand_image;
    $data->title=request('title');
    $data->about=request('about');
    $submit=$data->update();
  }

  else{
    $image = $request->file('logo');
    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
    $destinationPath = public_path('/images');
    $image->move($destinationPath, $input['imagename']);
    $brand_image=$input['imagename'];

    $data=new benifitabout;
    $data->image=$brand_image;
    $data->title=request('title');
    $data->about=request('about');
    $submit=$data->save();
  }


  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully submit the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}

public function benifitshow(){

  $data=benifitabout::all();
  return view('AdminPanel/ShowBenifit',['data'=>$data]);
}

public function shwbenifitshow($id){

  $show=benifitabout::all()->where('id','=',$id);
  return view('AdminPanel/editbenifit',['show'=>$show]);
}



public function editbenifitshow(){

 request()->validate([
 ],[


 ]);
 $image = $request->file('logo');
 $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
 $destinationPath = public_path('/images');
 $image->move($destinationPath, $input['imagename']);
 $brand_image=$input['imagename'];

 $data=benifitabout::find($id);
 $data->image=$brand_image;
 $data->title=request('title');
 $data->about=request('about');
 $submit=$data->save();
 if($submit==true){

  return Redirect::back()->with('success', 'Sucessfully update the data');
}else{
  return Redirect::back()->with('danger','error please check');
}
}

public function Available(){

 request()->validate([
 ],[


 ]);

 $image = $request->file('image');
 $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
 $destinationPath = public_path('/images');
 $image->move($destinationPath, $input['imagename']);
 $brand_image=$input['imagename'];

 $data=new AvailableStatus;
 ;

 $data->state=request('state');
 $data->pin=request('pincode');
 $data->image=$brand_image;
 $data->status=request('status');
 $submit=$data->save();
 if($submit==true){

  return Redirect::back()->with('success', 'Sucessfully submit the data');
}else{
  return Redirect::back()->with('danger','error please check');
}

}

public function showavailable(){

  $show=AvailableStatus::all();
  return view('AdminPanel/ShowAvailablePin',['show'=>$show]);

}



public function questionAnswer(Request $request){

 request()->validate([
 ],[


 ]);
 $data= new questionanswer;
 $data->title=$request->title;
 $data->about=$request->about;
 $data->save();

 return Redirect::back()->with('success','Sucessfully submit data'); 
}

public function showqa(){
  $show=questionanswer::all();
  return view('AdminPanel/showqa',['show'=>$show]);
}

public function getAllUsers(){
  $users=User::all();
  return view('AdminPanel.users-list',compact('users'));
}

public function getAllOrders(){
  $orders=appoinment::all();
  return view('AdminPanel.all-orders',compact('orders'));
}

public function getPrice(){
  $brands = brandDetails::all();
  $modals = array();
  $i=0;

  foreach($brands as $brand){
    $modal=brandModel::where('brand_id',$brand->id)->get();
    if(!empty($modal)){
      foreach($modal as $mod){
        $modals[$i++] = $mod;
      }
    }
  }

            // foreach($models as $m){
            //     echo $m;
            // }

            // die;
  return view('AdminPanel.Price',compact('modals'));
}

public function changeOrderStatus(Request $request){
  $id = $request->ID;
  $status = $request->status;
  $order = appoinment::find($id);
  $order->track_point = $status;
  $order->update();

  return response()->json(['status' => 'Updated Successfully']);
}

  public function getInvoice($id){

        $user=appoinment::find($id);

        return view('AdminPanel/invoice',compact('user'));
        }




        public function pdfview(Request $request)
    {
        // $users = DB::table("users")->get();
        // $users = DB::table("users")->get();
      // echo $request->order_id;
      // die;
      $id = $request->order_id;
      $user=appoinment::find($id);
      // echo $user;
      // die;
        view()->share('user',$user);

        if($request->has('download')){
          // Set extra option
          PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
          // pass view file
            $pdf = PDF::loadView('AdminPanel/invoice');
            // download pdf
            return $pdf->download('pdfview.pdf');
        }
        return view('AdminPanel/invoice');
    }







}
