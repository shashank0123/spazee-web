<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

     public function register(Request $request) 
    { 
        
        request()->validate([


        ],[

        ]);
        $otp = rand(100000, 999999);
        $otp_at=date('Y-m-d H:m:s');
        $phone=request('phone');
        $time=NOW();
        $data = array('mobile' =>$phone, 'otp'=>$otp, 'otp_at'=>$otp_at,'created_at'=>$time, 'updated_at'=>$time);
        
        $submit=DB::table('users')->insert($data);

        $success['token'] =  $submit->createToken('MyApp')-> accessToken; 
                    $success['phone'] =  $submit->phone;
            return response()->json(['success'=>$success], $this-> successStatus); 
}
}
