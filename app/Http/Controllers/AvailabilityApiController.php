<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use StdClass, Response;

class AvailabilityApiController extends Controller
{
    public function available_dates(Request $request)
    {
    	$dates=array();
    	$object = new StdClass;
    	$object->date = date('d M');
    	$object->timeslot = array('0' => "9:00 - 12:00",
    							  '1' => "12:00 - 16:00", 
    							  '2' => "16:00 - 18:00", 
    							);;
    	array_push($dates, $object);
    	array_push($dates, $object);
    	array_push($dates, $object);
    	array_push($dates, $object);
    	array_push($dates, $object);
    	array_push($dates, $object);
        return Response::json(array(
            'status' => 'success',
            'dates' => $dates),
            200
        );
    }
}
