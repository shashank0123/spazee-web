<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use StdClass, Response;

class ItemPriceController extends Controller
{
    public function estimated_price(Request $request)
    {
    	$brand_name = $request->brand_name;
    	$model_name = $request->model_name;
    	$location = $request->location;
    	$token 		= $request->token;
    	$status = 402;
    	if (!$brand_name || !$model_name || !$token || !$location){
    		return Response::json(array(
            'status' => 'failed'),
            400
        );


    	}
    	else{
	    	$cart = array(
	    					'market_price' => 'RS 10000',
	    					'discount' => 'RS 100',
	    					'our_price' => 'RS 9900',
	    					'total' => 'RS 9900',
	    					);

	    	$stats = array(
	    					'screens_repaired' => '200+', 
	    					'rated' => '4.1+', 
				    );

	    	$device = array(
	    					'device' => $model_name." (".$brand_name.")", 
	    					'location' => 'Delhi', 
				    );

	    	return Response::json(array(
            'status' => 'success',
            'cart' => $cart,
            'stats' => $stats,
            'device' => $device
	        ),
            200
        );


    	}
    }
}
