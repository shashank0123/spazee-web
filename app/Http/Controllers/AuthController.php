<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\selectcity;
use App\User;
use JWTAuth;
use Log;
use DB;
class AuthController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

   
  
   public function login(Request $request)
    {
       
        $rules = [
            'mobile' => 'required',
            'otp' => 'required',
        ];
        $user = User::where('mobile', $request->mobile)->where('otp', $request->otp)->first();
        
        $credentials = $request->only('mobile','otp');
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'message'=> "Please validate before sending"], 302);
        }
        try {
            // attempt to verify the credentials and create a token for the user
            if ($user){
                if (!$token = JWTAuth::attempt([
                                                'mobile' => $request->mobile,
                                                'password' => 'password@32'
                                                ])) {
                    return response()->json(['success' => false, 'message' => 'We cant find an account with this credentials.'], 401);
                }
            }
            else
                return response()->json(['success' => false, 'message' => 'We cant find an account with this credentials.'], 401);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'message' => 'Failed to login, please try again.'], 500);
        }
        // all good so return the token
        $user = User::where('mobile', $request->mobile)->first();
        return response()->json(['success' => true, 'user'=> [ 'token' => $token, 'user' => $user], 'message' => 'data received successfully'], 200);
    }
    
    public function me()
    {
        return response()->json($this->guard()->user());
    }

    
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

   
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

   
    public function guard()
    {
        return Auth::guard();
    }

    


    
}
