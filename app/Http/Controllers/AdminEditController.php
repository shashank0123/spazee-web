<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\slider;
use DB;
use Redirect;
use App\brandDetails;
use App\brandModel;
use App\brandcolor;
use App\contact;
use App\AboutUs;
use App\AvailableStatus;
use App\termsandcondition;
class AdminEditController extends Controller
{
    

	public function showbrandDetails(){
		$show=brandDetails::all();
		return view('/AdminPanel/editHome',['show'=>$show]);
	}
    public function editbrandDetails(Request $request){

    	request()->validate([
            
            'images' =>'mimes:jpeg,png,jpg,gif,svg|max:5000',
           
             ], [

                
            ]);

    	$id=request('id');
    	$image = $request->file('images');
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['imagename']);

        $brand_image=$input['imagename'];
    	$data=brandDetails::find($id);
    	$data->brand_name=request('brand_name');
    	$data->brand_image=$brand_image;

    	$save=$data->save();
    	if($save==true){

                    return Redirect::back()->with('success', 'Sucessfully update slider');
                }else{
                    return Redirect::back()->with('danger', 'error');

                }

    }

    public function showModel(){
    	$show=brandModel::all();
		return view('/AdminPanel/editModel',['show'=>$show]);


    }
    public function EditModelPage($id){
    	$show=brandModel::all()->where('id','=',$id);
		return view('/AdminPanel/editModelPage',['show'=>$show]);


    }

    public function editbrandModel(Request $request){

    	 request()->validate([
            'BrandModelImage'=>'required|mimes:jpeg,png,jpg,gif,svg|max:5000',
        ],[

        ]);

                $image = $request->file('BrandModelImage');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath, $input['imagename']);
                $brand_image=$input['imagename'];
                $id=request('id');
                $data=brandModel::find($id);
                $data->brand_id=request('brand_id');
                $data->brand_model_name=request('BrandModel');
                $data->brand_image=$brand_image;
                $data->actualprice=request('actualprice');
                $data->sellingprice=request('SellingPrice');
                $data->screenproPrice=request('ScreenproPrice');
                $data->about=request('about');
                 $submit=$data->save();

       if($submit==true){

        return Redirect::back()->with('success', 'Sucessfully submit the data');
       }else{
        return Redirect::back()->with('danger','error please check');
       }
    }

    public function brandcolor(){
    	$show=brandcolor::all();
		return view('/AdminPanel/editcolor',['show'=>$show]);


    }

    public function editbrandcolor(Request $request){


    	request()->validate([

        ],[


        ]);

    	$id=request('id');
        $data=brandcolor::find($id);

        $data->brand_id=request('brand_name');
        $data->brand_model_id=request('brand_model');
        $data->color=request('color');

         $submit=$data->save();

       if($submit==true){

        return Redirect::back()->with('success', 'Sucessfully submit the data');
       }else{
        return Redirect::back()->with('danger','error please check');
       }

    }


     public function editTerms(Request $request){

             request()->validate([
            ],[
            

            ]);
             $id=request('id');

             $data=termsandcondition::find($id);
            
             $data->about=$request->input('about');
             $submit=$data->save();
            if($submit==true){

        return Redirect::back()->with('success', 'Sucessfully update the data');
       }else{
        return Redirect::back()->with('danger','error please check');
       }

             
}

}
