<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use JWTAuth;
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Session;
use App\address;
use Hash;
use App\contact;
use App\contactUs;
use App\selectcity;
use Response;
use App\AvailableStatus;
class UserController extends Controller 
{
public $successStatus = 200;

    
 
    public function register12(Request $request) 
    { 
		
		$validator = Validator::make($request->all(), [ 
            'mobile'=>'required|min:10|unique:users', 
        ]);
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }		
        $otp = rand(100000, 999999);

		session(['otp'=>$otp]);
		$otp_at=date('Y-m-d H:m:s');
		$mobile=request('mobile');
		session(['mobile'=>$mobile]);
		$time=NOW();
		$data = array('mobile' =>$mobile, 'otp'=>$otp, 'otp_at'=>$otp_at,'created_at'=>$time, 'updated_at'=>$time);

                $otpen = str_replace(' ', '%20', $otp);
                $message = "This%20is%20your%20OTP%20$otpen%20to%20reset%20password%20of%20your%20indiancf%20account%20.Valid%20for%202%20hours%20";
                  $url = "http://103.247.98.91/API/SendMsg.aspx?uname=20190770&pass=d797g9t4&send=flgrth&dest=$mobile&msg=$message";    
                   $c = curl_init();
                   curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
                   curl_setopt($c,CURLOPT_HTTPGET ,1);
                   
                   curl_setopt($c, CURLOPT_URL, $url);
                   $contents = curl_exec($c);
                 if (curl_errno($c)) {
                   echo 'Curl error: ' . curl_error($c);
                 }else{
                   curl_close($c);
                 }
		
		$submit=DB::table('users')->insert($data);
		if($submit==true){

			return response()->json(['success'=>$data], $this-> successStatus); 
		}else{
			 return response()->json(['error'=>'something wrong']);
		}
}

	public function resendOtp(){
		
		//$mobile=request('mobile');//session::get('mobile');
		$mobile=$request->mobile;
		$otp = rand(100000, 999999);
		$otp_at=date('Y-m-d H:m:s');


		 $otpen = str_replace(' ', '%20', $otp);
                $message = "This%20is%20your%20OTP%20$otpen%20to%20reset%20password%20of%20your%20indiancf%20account%20.Valid%20for%202%20hours%20";
                  $url = "http://103.247.98.91/API/SendMsg.aspx?uname=20190770&pass=d797g9t4&send=flgrth&dest=$mobile&msg=$message";    
                   $c = curl_init();
                   curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
                   curl_setopt($c,CURLOPT_HTTPGET ,1);
                   
                   curl_setopt($c, CURLOPT_URL, $url);
                   $contents = curl_exec($c);
                 if (curl_errno($c)) {
                   echo 'Curl error: ' . curl_error($c);
                 }else{
                   curl_close($c);
                 }

             $updateOtp=User::find($mobile);
             $updateOtp->otp=$otp;
             $updateOtp->otp_at=$otp_at;
             if($updateOtp->save()==true){

			return response()->json(['success'=>$updateOtp]); 
		}else{
			return response()->json(['error'=>'something is wrong']); 

		}


	}

	public function verifyOtp(Request $request){
	
	$validator = Validator::make($request->all(), [ 
            'email'=>'required|unique:users',
            'name'=>'required',
            

        ]);
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }	

	$otp=request('otp');
	$mobile=request('mobile');
	$password='password@32';

	//$otp_get=session::get('otp');
	$userdata=User::all()->where('mobile','=',$mobile);
	foreach ($userdata as $key => $value) {
		$otp_get=$value->otp;
	}
	if($otp==$otp_get){

	$name= request('name');
	$email=request('email');
//	$password= str_random($length = 6);
	$Newpassword = Hash::make($password);
	
	$data=DB::update('update users set name=?, email=?, password=?, verified=1  where mobile=?',[$name,$email,$Newpassword,$mobile]);
	
	if($data==true){
     $user  = User::where([['mobile','=',request('mobile')],['otp','=',request('otp')]])->first();
      $token= Auth::login($user);
		    return response()->json(['name' => $name,'email'=>$email,'mobile'=>$mobile,'token'=>$token]);
		
		}else
		 return response()->json(['error'=>'failed']); 
	
}else{
		 return response()->json(['error'=>'Wrong otp' ]); 

}
}

public function userdetails(){
		$id=Auth::user()->id;
		$user =Auth::user();
    $address=address::all()->where('user_id','=',$id);
		
  return response()->json(['userdata' => $user, 'address'=>$address]); 
  

	}

	public function UserAddress(Request $request){
		$validator = Validator::make($request->all(), [ 
            
        ]);
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

		$id=Auth::user()->id;
		//$id=1;
		$data= new address;
		$data->user_id=$id;
		$data->location=request('location');
		$data->completeAddress=request('completeAddress');
		$data->landmark=request('landmark');
		$data->pincode=request('pincode');
		$data->city=request('city');
		$data->state=request('state');
		$data->saveAs=request('saveAs');
		$submit=$data->save();

		if($submit==true){

			return response()->json([
                'success' => $data
            ]);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'something error'
            ], 401);
		}

		}
		
	public function updateaddress(Request $request){
	    
	    	$validator = Validator::make($request->all(), [ 
            
        ]);
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        $field=$request->field;

		$id=Auth::user()->id;
		$location=request('location');
		$completeAddress=request('completeAddress');
		$landmark=request('landmark');
		$pincode=request('pincode');
		$city=request('city');
		$state=request('state');
		$saveAs=request('saveAs');
		$data = array(
			'user_id'=>$id,
			'location' =>$location ,
			'completeAddress'=>$completeAddress,
			'landmark'=>$landmark,
			'pincode'=>$pincode,
			'city'=>$city,
			'state'=>$state,
			'saveAs'=>$saveAs,

		 );

		$submit=DB::update('update address set location=?,completeAddress=?,landmark=?,pincode=?,city=?,state=?,saveAs=? where user_id=? AND saveAs=? ',[$location,$completeAddress,$landmark,$pincode,$city,$state,$saveAs,$id,$field]);
		if($submit==true){

			return response()->json([
                'success' => $data,
                'message'=>'update data',
            ]);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'something error'
            ], 401);
		}


	}
	
	public function deleteaddress(Request $request){
	    $field=$request->field;
	    $id=Auth::user()->id;
	    
	    $delete1=address::where('user_id','=',$id)->where('saveAs','=',$field)->delete();
	    if($delete1==true){
	    return response()->json(['Success'=>'delete address successfully']);
	    }else{
	         return response()->json(['Field'=>'address not deleted']);
	    }
	   
	}

		public function authdata(){
			
			$data=Auth::user();
			return response()->json([
				'success'=>$data
			]);
		}


		public function contactUs(Request $request){

			$validator = Validator::make($request->all(), [ 
            'email'=>'required|email',
            'name'=>'required',
            
        ]);
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $data= new contactUs;
        $data->name=$request->name;
        $data->email=$request->email;
        $data->phone=$request->mobile;
        $data->question=$request->question;
        $data->message=$request->message;
        $submit=$data->save();
        if($submit==true){
            return response()->json(['success'=>$data]);            

        }else{
            return response()->json(['error'=>'something is error']);            


        }


		}

	public function action(Request $request)
    {
     $validation = Validator::make($request->all(), [
      'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
     ]);
     if($validation->passes())
     {
      $image = $request->file('select_file');
      $new_name = rand() . '.' . $image->getClientOriginalExtension();
      $image->move(public_path('images'), $new_name);
      return response()->json([
       'message'   => 'Image Upload Successfully',
       'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
       'class_name'  => 'alert-success'
      ]);
     }
     else
     {
      return response()->json([
       'message'   => $validation->errors()->all(),
       'uploaded_image' => '',
       'class_name'  => 'alert-danger'
      ]);
     }
    }


    public function selectcity(Request $request){

     

      $city=new selectcity;

      $city->select_city=$request->city;
      $city->brow_token=$request->token;
      $city->save();
      return response::json(['city'=>$city]);

    }

     public function sendotp(Request $request){

        $otp = rand(100000, 999999);
        $otp_at=date('Y-m-d H:m:s');
        $mobile=$request->mobile;
       
                $otpen = str_replace(' ', '%20', $otp);
                $message = "This%20is%20your%20OTP%20$otpen%20to%20reset%20password%20of%20your%20indiancf%20account%20.Valid%20for%202%20hours%20";
                  $url = "http://103.247.98.91/API/SendMsg.aspx?uname=20190770&pass=d797g9t4&send=flgrth&dest=$mobile&msg=$message";    
                   $c = curl_init();
                   curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
                   curl_setopt($c,CURLOPT_HTTPGET ,1);
                   
                   curl_setopt($c, CURLOPT_URL, $url);
                   $contents = curl_exec($c);
                 if (curl_errno($c)) {
                   echo 'Curl error: ' . curl_error($c);
                 }else{
                   curl_close($c);
                 }
        $user=DB::update('update users set otp=?, otp_at=? where mobile=?',[$otp,$otp_at,$mobile]);
        if($user==true){
       
        return response()->json(['success'=>$mobile,'otp'=>$otp]);
      }else{
        return response()->json(['fail'=>$user]);

    }
      }
      
      public function profileupdate(Request $request){
          	$validator = Validator::make($request->all(), [ 
            'email'=>'required|email',
            'name'=>'required',
            
        ]);
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

          $name=$request->name;
          $email=$request->email;
          $mobile=Auth::user()->mobile;
          $id=Auth::user()->id;
          
          $user=User::find($id);
          $user->name=$name;
          $user->email=$email;
          $user->save();
          
          return Response::json(['success'=>'user']);
      }
      
      public function locationstatus(Request $request){
          $id=Auth::user()->id;
          //$id=4;
          $field=$request->field;
          $address=address::all()->where('user_id','=',$id)->where('saveAs','=',$field);
          foreach($address as $key => $value){
              $pincode=$value->pincode;
          
          
          $avail=AvailableStatus::all()->where('pincode','=',$pincode)->where('status','=','Available');
          foreach($avail as $key => $value){
              $status=$value->pincode;
          }
          if($pincode==$status){
              return Response::json(['success'=>'available']);
          }else{
               return Response::json(['success'=>'Not available']);
          }
          
          
      }
      }

}
