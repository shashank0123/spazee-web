<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class address extends Model
{
    protected $table='address';

    protected $fillable = ['user_id','location','completeAddress','landmark','
    pincode','city','state','saveAs'];
}
