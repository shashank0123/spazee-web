<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvailableStatus extends Model
{
    protected $table='_state_available';
    protected $fillable = ['state', 'pin','image', 'status'];
}
