<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class appoinment extends Model
{
    protected $table='appoinment';

    protected $fillable=['username','email','phone','Brand_Name','Brand_Model','Brand_color','price','address','pincode','service_date','service_time'];
}
