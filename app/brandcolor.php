<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class brandcolor extends Model
{
   
	protected $table='brand_color';

	protected $fillable=['brand_id','brand_model_id','color'];

	public function brandDetails()
    {
        return $this->hasMany('App\brandModel');
    }

}
