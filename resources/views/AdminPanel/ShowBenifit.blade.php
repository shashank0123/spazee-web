@extends('layouts.Dashboard')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<div class="container">
     <div style="text-align: center;" ><h3><u><b>Slider  </b></u></h3></div>
     <div class="form-group">
                
             </div>
    <div class="table-responsive ">
        <!--Table-->
        <table class="table table-striped table-bordered" style="width: 88%; overflow: auto">

          <thead style="background: #0066ff; color: #fff">
  
    <tr>

      <th scope="col">S.no.</th>
      <th scope="col">Image</th>
      <th scope="col">Title</th>
      <th scope="col">About</th>

      <td scope="col">Edit</td>
      <!-- <td scope="col">Delete</td> -->

     </tr>
  </thead>
  @forelse($data as $row)
  <tbody>
  	
  	<tr scope="col" style="background: #e6f2ff;"> 

  		<td>{{ $loop->iteration }}</td>
      <td><img src="/images/{{$row->image}}" width="60px" height="60px"></td>
      <td>{{ $row->title }}</td>
      <td>{!!$row->about!!}</td>

      <td><a href="/admin/benifit/edit/{{$row->id}}" class="btn btn-danger">Edit</a></td>
  		
     <!--  <td><a href="/admin/slider-image/delete/{{$row->id}}" class="btn btn-danger">Delete</a></td> -->

	</tr>
  	
  </tbody>
 @empty
    <p style="color: red;">No Slider Image</p>
  @endforelse 
</table>
	</div>
	</div>




@endsection