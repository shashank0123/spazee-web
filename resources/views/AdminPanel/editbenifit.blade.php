@extends('layouts.Dashboard')

@section('content')
 <div class="col-sm-12" style="border:5px solid black;">
 	<h3 style="text-align: center;"><u><b>Edit-Benifits Add</b></u></h3>
 	<br>
 	@foreach($show as $row)
 	<form action="/admin/benifitsterms" method="post" enctype="multipart/form-data">
 	 	
	@csrf
<input type="hidden" value="{{$row->id}}" name="id" id="id">
	<div class="form-group">
		<label>Upload Logo</label>
  		<input type="file" name="logo" value="{{$row->image}}" class="form-control" required="" placeholder="upload logo">
  	</div>
  <div class="form-group">
  	<label>Title</label>
  	<input type="text" name="title"  value="{{$row->title}}" class="form-control" required="" placeholder=" about title">
  </div>
  
  <div class="form-group">
  	 <textarea class="textarea" name="about" rows="6" placeholder="Write something about model" >{{$row->about}}</textarea>
                
  </div>
  <div>
<button class="btn btn-success" type="submit">submit</button>
</div>

<br>
</div>
</div>
@endforeach

<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
  <script>
            
            CKEDITOR.replace( 'about' );
            CKEDITOR.replace( 'about' );

</script>
@endsection