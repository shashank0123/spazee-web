<?php
use App\brandDetails;
use App\brandModel;
?>

@extends('layouts.Dashboard')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<div class="container">
     <div style="text-align: center;" ><h3><u><b>Users List</b></u></h3></div>
     <div class="form-group">
                
             </div>
    <div class="table-responsive ">
        <!--Table-->
        <table class="table table-striped table-bordered" style="width: 88%; overflow: auto">

          <thead style="background: #0066ff; color: #fff">
  
    <tr>

      <th scope="col">S.no.</th>
      <th scope="col">User Name</th>
      <th scope="col">Phone</th>
      <th scope="col">Email</th>
      <th scope="col">Joining Date</th>
      {{-- <td scope="col">Edit</td> --}}
      {{-- <td scope="col">Delete</td> --}}

     </tr>
  </thead>
  <?php $i=1; ?>
  @forelse($users as $user)
  <tbody>
  	
  	<tr scope="col" style="background: #e6f2ff;"> 

  		<td>{{ $i++ }}</td>
     
      <td>{{$user->name}} </td>

      
      <td>{{$user->mobile}}</td>
      <td>{{$user->email}}</td>
      <td>{{$user->created_at}}</td>
     {{-- <td>
      <button type="button" class="btn btn-primary" href="/admin/edit-user-detail/{{$user->id}}">Edit</button>

      <button type="button" data-myid="{{$user->id}}" data-myname="{!!$user->name!!}" data-mymodel="{{$user->brand_model_name}}" data-myemail="{{$user->email}}" data-mymobile="{{$user->mobile}}"  class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Edit</button></td> --}}
      {{-- <td><button type="button" href="/admin/user-lists/{{$user->id}}" class="btn btn-danger">Delete</a></td> --}}

	</tr>
  	
  </tbody>
 @empty
    <p style="color: red;">No  Details</p>
  @endforelse 
</table>
	</div>
	</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/admin/brand_name-edit" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            
            <input type="hidden" name="id" class="form-control" id="id">
          </div>
          <div class="form-group">
            
            <input type="text" name="brand_name" class="form-control" id="name">
          </div>
          <div class="form-group">
            
            <input type="text" name="brand_model_name" class="form-control" id="Model">
          </div>
           <div class="form-group">
           
            <input type="file" name="images" class="form-control" id="email">
          </div>
         
          
          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit"  class="btn btn-primary">Update</button>
      </div>
        </form>
      </div>
     
    </div>
  </div>
</div>   

<script type="text/javascript">
$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('myid')
  var name = button.data('mybrand')
  var email = button.data('myemail') 
  var model = button.data('mymodel') 
  var about = button.data('myabout') 

  var modal = $(this)
 
  modal.find('.modal-body #id').val(recipient)
   modal.find('.modal-body #email').val(email)
   modal.find('.modal-body #name').val(name)
   modal.find('.modal-body #Model').val(model)
   modal.find('.modal-body #about').val(about)



})


    </script>
   
    
@endsection