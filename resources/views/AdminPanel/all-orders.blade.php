<?php
use App\brandDetails;
use App\brandModel;

?>

@extends('layouts.Dashboard')

@section('content')
<style>

</style>
<meta name="csrf-token" value="{{csrf_token()}}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<div class="container">
 <div style="text-align: center;" ><h3><u><b>Orders List</b></u></h3></div>
 <div class="form-group">

 </div>
 <div class="table-responsive ">
  <!--Table-->
  <table class="table table-striped table-bordered" style="width: 88%; overflow: auto">

    <thead style="background: #0066ff; color: #fff">

      <tr>

        <th scope="col">S.no.</th>
        <th scope="col">Customer Name</th>
        <th scope="col">Email </th>
        <th scope="col">Mobile</th>
        <th scope="col">Brand Name</th>
        <th scope="col">Brand Model</th>
        <th scope="col">Brand color</th>
        <th scope="col">Price</th>
        <th scope="col">Service Date</th>
        <th scope="col">Service Time</th>    
        <th scope="col">Status</th>
        <th scope="col">Change Status</th>

      </tr>
    </thead>
    <?php $i=1; ?>
    @forelse($orders as $order)
    <tbody>

     <tr scope="col" style="background: #e6f2ff;"> 
      
      <td>{{ $i++ }}</td>       
      <td>{{$order->username}}</td>      
      <td>{{$order->email}}</td>
      <td>{{$order->phone}}</td>
      <td>{{$order->Brand_Name}}</td>
      <td>{{$order->Brand_Model}}</td>
      <td>{{$order->Brand_color}}</td>
      <td>Rs. {{$order->price}}</td>
      <td>{{$order->service_date}}</td>
      <td>{{$order->service_time}}</td>
      <td>
        <form method="POST">
          {{-- {{csrf --}}
          {{-- {{ csrf_token() }} --}}
        <select name="track_point" class="form-control" id="order_status" onchange="changeStatus({{$order->id}})">

          <option value="Order_cancelled" <?php if($order->track_point=="Order_cancelled") {echo 'selected';} ?> >Cancelled</option>
          <option value="Confirm_Order" <?php if($order->track_point=="Confirm_Order") {echo 'selected';} ?> >Confirmed</option>
          <option value="agent_assign" <?php if($order->track_point=="agent_assign") {echo 'selected';} ?> >Agent Assigned</option>
          <option value="delivery" <?php if($order->track_point=="delivery") {echo 'selected';} ?> >Delivery</option>

        </select> 

      </form>
        </td>
        <td>{{$order->service_time}}</td>
        <td>
          <form method="GET" action="{{ route('generate-pdf',['download'=>'pdf']) }}" >
            <input type="hidden" value="{{$order->id}}" name="order_id">
            <input type="submit" class="btn btn-primary" value="Invoice" >
          </form>'
        </td>
        
        {{-- <td> --}}
          {{-- <button type="button" class="btn btn-primary" href="/admin/edit-order/{{$order->id}}">Edit</button> --}}

          {{-- <button type="button" data-myid="{{$order->id}}" data-myname="{!!$order->name!!}" data-mymodel="{{$order->brand_model_name}}" data-myemail="{{$order->email}}" data-mymobile="{{$order->mobile}}"  class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Edit</button></td> --}}
      {{-- <td><a href="/admin/user-lists/{{$user->id}}" class="btn btn-danger">Delete</a></td>
      --}}
    </tr>

  </tbody>
  @empty
  <p style="color: red;">No  Details</p>
  @endforelse 
</table>
</div>
</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/admin/brand_name-edit" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">

            <input type="hidden" name="id" class="form-control" id="id">
          </div>
          <div class="form-group">

            <input type="text" name="brand_name" class="form-control" id="name">
          </div>
          <div class="form-group">

            <input type="text" name="brand_model_name" class="form-control" id="Model">
          </div>
          <div class="form-group">

            <input type="file" name="images" class="form-control" id="email">
          </div>

          
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit"  class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>   

<script type="text/javascript">
  $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('myid')
  var name = button.data('mybrand')
  var email = button.data('myemail') 
  var model = button.data('mymodel') 
  var about = button.data('myabout') 

  var modal = $(this)

  modal.find('.modal-body #id').val(recipient)
  modal.find('.modal-body #email').val(email)
  modal.find('.modal-body #name').val(name)
  modal.find('.modal-body #Model').val(model)
  modal.find('.modal-body #about').val(about)

});

function changeStatus(id){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('value');
  var orderID = id;
  var status = $('#order_status').val();
  alert(id+" "+status);

  $.ajax({
    url:"/admin/change-status",
    type: 'POST',
    data: {_token: CSRF_TOKEN, ID: orderID, status: status},

    success:function(data){
      
        window.location.href = "/admin/orders";
     
    }
  });
}
 
</script>


@endsection