@extends('layouts.Dashboard')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<div class="container">
     <div style="text-align: center;" ><h3><u><b>Available States  </b></u></h3></div>
     <div class="form-group">
                
             </div>
    <div class="table-responsive ">
        <!--Table-->
        <table class="table table-striped table-bordered" style="width: 88%;overflow: auto">

          <thead style="background: #0066ff; color: #fff">
  
    <tr>

      <th scope="col">S.no.</th>
      <th scope="col">State</th>
      <th scope="col">Pin</th>
      <th scope="col">Status</th>

      <td scope="col">Active/Inactive</td>
      <!-- <td scope="col">Delete</td> -->

     </tr>
  </thead>
  @forelse($show as $row)
  <tbody>
  	
  	<tr scope="col" style="background: #e6f2ff;"> 

  		<td>{{ $loop->iteration }}</td>
      <td>{{$row->state}}</td>
      <td>{{ $row->pin }}</td>
      <td>{{$row->status}}</td>

      <td><a href="" class="btn btn-danger">Activate</a></td>
  		
     <!--  <td><a href="/admin/slider-image/delete/{{$row->id}}" class="btn btn-danger">Delete</a></td> -->

	</tr>
  	
  </tbody>
 @empty
    <p style="color: red;">No states</p>
  @endforelse 
</table>
	</div>
	</div>




@endsection