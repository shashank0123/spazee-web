<?php
use App\brandDetails;

?>

@extends('layouts.Dashboard')

@section('content')
<script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/bootstrap-3.min.css">

 <div class="container-fluid" style="background: #fff" >
 	<div class="row" style="border:5px solid black;"style="background: #fff" >
 	<div class="col-sm-6" >
 	<h3 style="text-align: center;"><u><b>Upload Homepage Slider</b></u></h3>
 	 	<form action="/admin/slider" method="post" enctype="multipart/form-data">
 	 		@csrf
  <div class="form-group">
    <label for="exampleFormControlSelect1">Upload Slider Image</label>
    <input type="file" name="images" class="form-control" id="exampleFormControlInput1" placeholder="Upload Slider Image" required="">
    <h6>&nbsp;&nbsp;Image size should be 1400*510</h6>
  </div>

  <div>
  	<button type="submit" class="btn btn-success"> Submit</button>
  </div>
 <br>
</form>
 </div>
 <div class="col-sm-6"  >
  <div style="padding-top: 80px;">
   <a href="/admin/slider-image" class="btn btn-info"  style="width:30%; ">Edit</a></div>
 </div>
</div>
<br>
<div class="row" style="border:5px solid black;" >
	<div class="col-sm-4">
		<h3 style="text-align: center;"><u><b>Add Brand Name</b></u></h3>

 	<form action="/admin/brandname" method="post" enctype="multipart/form-data">
 	 		@csrf
  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Name</label>
    <input type="text" class="form-control" name="BrandName" id="exampleFormControlInput1" placeholder="Type Mobile Brand Name" required="">
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Logo </label>
    <input type="file" class="form-control" name="BrandLogo" id="exampleFormControlInput1" placeholder="Type Mobile Brand Name" required="">
    
  </div>
 
<button class="btn btn-success" type="submit">submit</button>
<a href="/showbrandDetails" class="btn btn-info" >edit</a>

 
	</form>

</div>

  <!-- brand available status -->
  <div class="col-sm-8">
<h3 style="text-align: center;"><u><b>Add Brand Model</b></u></h3>

  <form action="/admin/brandmodel" method="post" enctype="multipart/form-data">
      @csrf
  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Name</label>
    <select name="brand_id" class="form-control">

     <?php
      $data=brandDetails::all();
      foreach ($data as $key => $value) {
        
      
      ?>
      <option value="{{$value->id}}">{{$value->brand_name}}</option>
      <?php }?>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Model</label>
    <input type="text" class="form-control" name="BrandModel"  placeholder="Type Mobile Model Name" required="">
    
  </div>
   <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Model Image</label>
    <input type="file" class="form-control" name="BrandModelImage"  placeholder="choose brand model image" required="">
    
  </div>

  <div class="form-group">
    <label for="exampleFormControlSelect1">Actual Price</label>
    <input type="text" class="form-control" name="actualprice"  placeholder="actualprice" required="">
    
  </div>

   <div class="form-group">
    <label for="exampleFormControlSelect1">Selling Price</label>
    <input type="text" class="form-control" name="SellingPrice"  placeholder="Selling Price" required="">
    
  </div>

  <div class="form-group">
    <label for="exampleFormControlSelect1">ScreenPro Price</label>
    <input type="text" class="form-control" name="ScreenproPrice"  placeholder="Screenpro Price" required="">
    
  </div>
   
  <div class="form-group">
    <label>text about model(*optional)</label>
     <textarea class="textarea" name="about" rows="3" placeholder="Write something about model"
                             ></textarea>
                
  </div>
  <div>
<button class="btn btn-success" type="submit">submit</button>
<a href="/showModel" class="btn btn-info" >edit</a>

</div>

<br>
</form>

</div>
</div>

<hr>
 <div class="col-sm-12" style="border:5px solid black;">
 	<div class="col-sm-6">
    <h3 style="text-align: center;"><u><b>Available Status </b></u></h3>
  
  <form action="/admin/AvailableStatus" method="post" enctype="multipart/form-data">
      @csrf
  
  <div class="form-group">
    <label for="exampleFormControlSelect2">Available States</label>
<select name="state" class="form-control" required="">
<option value="">Select State</option>
<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
<option value="Andhra Pradesh">Andhra Pradesh</option>
<option value="Arunachal Pradesh">Arunachal Pradesh</option>
<option value="Assam">Assam</option>
<option value="Bihar">Bihar</option>
<option value="Chandigarh">Chandigarh</option>
<option value="Chhattisgarh">Chhattisgarh</option>
<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
<option value="Daman and Diu">Daman and Diu</option>
<option value="Delhi">Delhi</option>
<option value="Goa">Goa</option>
<option value="Gujarat">Gujarat</option>
<option value="Haryana">Haryana</option>
<option value="Himachal Pradesh">Himachal Pradesh</option>
<option value="Jammu and Kashmir">Jammu and Kashmir</option>
<option value="Jharkhand">Jharkhand</option>
<option value="Karnataka">Karnataka</option>
<option value="Kerala">Kerala</option>
<option value="Lakshadweep">Lakshadweep</option>
<option value="Madhya Pradesh">Madhya Pradesh</option>
<option value="Maharashtra">Maharashtra</option>
<option value="Manipur">Manipur</option>
<option value="Meghalaya">Meghalaya</option>
<option value="Mizoram">Mizoram</option>
<option value="Nagaland">Nagaland</option>
<option value="Orissa">Orissa</option>
<option value="Pondicherry">Pondicherry</option>
<option value="Punjab">Punjab</option>
<option value="Rajasthan">Rajasthan</option>
<option value="Sikkim">Sikkim</option>
<option value="Tamil Nadu">Tamil Nadu</option>
<option value="Tripura">Tripura</option>
<option value="Uttaranchal">Uttaranchal</option>
<option value="Uttar Pradesh">Uttar Pradesh</option>
<option value="West Bengal">West Bengal</option>
</select>
    
  </div>
  <div class="form-group">
    <label>Pincode</label>
    <input type="number" name="pincode" class="form-control" placeholder="Enter pin for available states" required="">
  </div>
  <div class="form-group">
    <label>Image</label>
    <input type="file" name="image" class="form-control" required="">
  </div>
  <div class="form-group">
    <label>Status</label>
    <select name="status" class="form-control" required="" >
      <option>Available</option>
      <option>Not Available</option>

    </select>
  </div>

  <div>
    <button type="submit" class="btn btn-success"> Submit</button>
<a href="/admin/show/AvailableStatus" class="btn btn-info" >View</a>

  </div>
</form>
 <br>
 </div>
  
  <div class="col-sm-6" style="border-left: 2px solid black; height:405px
  ">
    <h3 style="text-align: center;"><u><b>Add Brand color</b></u></h3>

  <form action="/admin/brandcolor" method="post" enctype="multipart/form-data">
      @csrf
  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Name</label>
    <select name="brand_name" class="form-control">
       <option>select</option>
       <?php
      $data=brandDetails::all();
      foreach ($data as $key => $value) {
        
      
      ?>

      
      <option value="{{$value->id}}">{{$value->brand_name}}</option>
      <?php }?>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Model </label>
   <select name="brand_model" required="" class="form-control">
    <!-- <option>select</option> -->
   </select>
    
  </div>
   <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Color</label>
    <input type="text" class="form-control" name="color"  placeholder="Brand color" required="">
    
  </div>

  <div>
<button class="btn btn-success" type="submit">submit</button>
<a href="/brandcolor-page" class="btn btn-info">edit</a>

</div>
</form>
  </div>

</div>


<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
  <script>
            
            CKEDITOR.replace( 'about' );
            CKEDITOR.replace( 'about' );

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="brand_name"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/myform/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="brand_model"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="brand_model"]').append('<option value="'+ value.brand_id+'">'+ value.brand_model_name +'</option>');
                        });

                    }
                });
            }else{
                $('select[name="brand_model"]').empty();
            }
        });
    });
</script>

@endsection


