@extends('layouts.Dashboard')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<div class="container">
     <div style="text-align: center;" ><h3><u><b>Edit Brand Name</b></u></h3></div>
     <div class="form-group">
                
             </div>
    <div class="table-responsive ">
        <!--Table-->
        <table class="table table-striped table-bordered">

          <thead style="background: #0066ff; color: #fff">
  
    <tr>

      <th scope="col">S.no.</th>
      <th scope="col">Name</th>
      <th scope="col">Image</th>
      <td scope="col">Edit</td>
      <td scope="col">Delete</td>

     </tr>
  </thead>
  @forelse($show as $row)
  <tbody>
  	
  	<tr scope="col" style="background: #e6f2ff;"> 

  		<td>{{ $loop->iteration }}</td>
      <td>{{ $row->brand_name }}</td>
      <td><img src="/images/{{$row->brand_image}}" width="60px" height="60px"></td>
  		<td><button type="button" data-myid="{{$row->id}}" data-myname="{{$row->brand_name}}"  class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Edit</button></td>
      <td><a href="/admin/brand_details/{{$row->id}}" class="btn btn-danger">Delete</a></td>

	</tr>
  	
  </tbody>
 @empty
    <p style="color: red;">No  Details</p>
  @endforelse 
</table>
	</div>
	</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/admin/brand_name-edit" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            
            <input type="text" name="id" class="form-control" id="id">
          </div>
          <div class="form-group">
            
            <input type="text" name="brand_name" class="form-control" id="name">
          </div>
           <div class="form-group">
           
            <input type="file" name="images" class="form-control" id="email">
          </div>
          
          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit"  class="btn btn-primary">Update</button>
      </div>
        </form>
      </div>
     
    </div>
  </div>
</div>   

<script type="text/javascript">
$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('myid')
  var name = button.data('myname')
  var email = button.data('myemail') // Extract info from data-* attributes
  var modal = $(this)
 
  modal.find('.modal-body #id').val(recipient)
   modal.find('.modal-body #email').val(email)
   modal.find('.modal-body #name').val(name)

})


    </script>
    
@endsection