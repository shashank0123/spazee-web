<?php 
use App\brandDetails;
use App\brandModel;
$i=1;
$dbValue = $user->id; 
$dbValue = "SPZME/".str_pad($dbValue, 5, "0", STR_PAD_LEFT); // it will produce F-0000001;


function convert_number_to_words($number) {
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );
    if (!is_numeric($number)) {
        return false;
    }
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }
    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
    $string = $fraction = null;
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
    return $string;
}
?>















<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<style>

	table tr { border-bottom: 1px solid #000 !important; }
	.invoice { text-align: center; width: 98%; }
	 #border-mid  { border-right: 1px solid #000; }
	 	 table td, table th { width: 10% !important ; padding: 15px  }
	 #head { background-color: #0f95f4 }
	
.consignee-head,.receiver-head { text-align: center; }
	.invoice-data , .service-date , .receiver-desc , .consignee-desc  { margin:10px 10px ; line-height: 1.5}

	.address { text-align: right; margin-top: 15px }
	.address p { line-height: 0.7;color: #fff; }

	#logo-set { width: 60%; height: auto }
	#table-head th{ padding: 10px !important  }
	#table-data td{ padding: 10px !important }
	#total-bill { text-align: right; }

	#person-head { text-align: center; }


</style>

<div class="container invoice" id="html-2-pdfwrapper">

	<table class="table-bordered">
		<tr id="head">
			<td colspan='10'>
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<div class="logo">
								<image src="/logo/spazelogo.png" id="logo-set" />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="address">

								<p>Plot No  20-B Sangam Vihar Ghaziabad</p>
								<p>CIN : UP29D001117</p>
								<p>Phone : 8470095700</p>
								<p>Email us: support@spazeme.com</p>

							</div>
						</div>
					</div>
				</div>
			</td>
		</tr>

		<tr>
			<td colspan="5" id="border-mid">
				<div class="invoice-data">
					<div class="row">
						<div class="col-sm-6 col-xs-6">Invoice Number</div>
						<div class="col-sm-6 col-xs-6">: {{$dbValue}}</div>
						<div class="col-sm-6 col-xs-6">Invoice Date</div>
						<div class="col-sm-6 col-xs-6">: <?php echo date('d-m-Y'); ?></div>
						<div class="col-sm-6 col-xs-6">Tax is payable on Reserve Charge</div>
						<div class="col-sm-6 col-xs-6"> </div>
						{{-- <div class="col-sm-1 col-xs-6">PAN</div>
						<div class="col-sm-5 col-xs-6">: </div>
						<div class="col-sm-1 col-xs-6">CIN</div>
						<div class="col-sm-5 col-xs-6">: </div>			 --}}	
					</div>
				</div>
			</td>
			<td colspan="5">
				<div class="service-date">
					<div class="row">
						<div class="col-sm-6 col-xs-6">Service Type</div>
						<div class="col-sm-6 col-xs-6">: Screen Repair</div>
						<div class="col-sm-6 col-xs-6">Order ID</div>
						<div class="col-sm-6 col-xs-6">: {{$user->order_id}}</div>
						<div class="col-sm-6 col-xs-6">Date & Time of Service</div>
						<div class="col-sm-6 col-xs-6">: {{$user->service_time}}</div>
						<div class="col-sm-6 col-xs-6">Place of Supply</div>
						<div class="col-sm-6 col-xs-6">: {{$user->field}}</div>
					</div>				
				</div>
			</td>
		</tr>

		<tr id="person-head">
			<th colspan="5" id="border-mid"><div class="receiver-head">Details of Receiver (Billed To)</div></th>
			<th colspan="5"><div class="consignee-head">Details of Consignee (Shipped To)</div></th>
		</tr>

		<tr>
			<td colspan="5" id="border-mid">
				<div class="receiver-desc">
					<div class="row">
						<div class="col-sm-4 col-xs-6">Name </div>
						<div class="col-sm-8 col-xs-6">: {{ucfirst($user->username)}}</div>
						<div class="col-sm-4 col-xs-6">Mobile</div>
						<div class="col-sm-8 col-xs-6">: {{$user->phone}}</div>
						<div class="col-sm-4 col-xs-6">Email</div>
						<div class="col-sm-8 col-xs-6">: {{$user->email}}</div>
					</div>
				</div>
			</td>
			<td colspan="5">
				<div class="consignee-desc">
					<div class="col-sm-4 col-xs-6">Name </div>
					<div class="col-sm-8 col-xs-6">: {{ucfirst($user->username)}}</div>
					<div class="col-sm-4 col-xs-6">Address</div>
					<div class="col-sm-8 col-xs-6">: {{ucfirst($user->address)}}</div>
					<div class="col-sm-4 col-xs-6">GSTIN</div>
					<div class="col-sm-8 col-xs-6">: 5%</div>
				</div>
			</td>
		</tr>

		<tr id="table-head">
			<th colspan="1">S. No.</th>
			<th colspan="1">Brand</th>
			<th colspan="1">Model</th>
			<th colspan="1">Color</th>
			<th colspan="1">Market Price</th>
			<th colspan="1">Spazeme Price</th>
			<th colspan="1">Discount</th>
			<th colspan="1">GST</th>
			<th colspan="2">Total</th>
			
		</tr>

		
		<tr id="table-data">
			<?php $brand_id = brandDetails::where('brand_name',$user->Brand_Name)->first();
				//echo $brand_id;
				//die;
		$model = brandModel::where('brand_id',$brand_id->id)->where('brand_model_name',$user->Brand_Model)->first();
		$total = $model->actualprice - $model->screenproPrice;
		$total_gst = ($total * 5)/100;
		$total = $total + $total_gst;
		?>
			<td colspan="1">{{$i++}}</td>
			<td colspan="1">{{$user->Brand_Name}}</td>
			<td colspan="1">{{$user->Brand_Model}}</td>
			<td colspan="1">{{$user->Brand_color}}</td>
			<td colspan="1">RS {{$model->actualprice}}</td>
			<td colspan="1">{{$user->price}}</td>
			<td colspan="1">RS {{$total}}</td>
			<td colspan="1">RS {{$total_gst}}</td>
			<td colspan="2">{{$user->price}}</td>
			
		</tr>

		<tr>
			<th colspan="10" id="total-bill"><h4><b>Total : RS {{$total}}</b></h4>
				<h5>{{strtoupper(convert_number_to_words($total))}} ONLY</h5>
			</th>
		</tr>
	</table>

</div>

