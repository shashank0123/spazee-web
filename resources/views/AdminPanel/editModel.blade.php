<?php
use App\brandDetails;
?>

@extends('layouts.Dashboard')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<div class="container">
     <div style="text-align: center;" ><h3><u><b>Edit Brand Model</b></u></h3></div>
     <div class="form-group">
                
             </div>
    <div class="table-responsive ">
        <!--Table-->
        <table class="table table-striped table-bordered" style="width: 88%; overflow: auto">

          <thead style="background: #0066ff; color: #fff">
  
    <tr>

      <th scope="col">S.no.</th>
      <th scope="col">Brand Name</th>
      <th scope="col">Brand Model Name</th>
      <th scope="col">Image</th>
      <th scope="col">Actual Price</th>
      <th scope="col">Selling Price</th>
      <th scope="col">Screen Pro Price</th>
      <th scope="col">About</th>
      <td scope="col">Edit</td>
      <td scope="col">Delete</td>

     </tr>
  </thead>
  @forelse($show as $row)
  <tbody>
  	
  	<tr scope="col" style="background: #e6f2ff;"> 

  		<td>{{ $loop->iteration }}</td>
      <?php 
             $user_id=$row['brand_id'];
            
      $data=brandDetails::all()->where('id','=',$user_id) ?>
      <td><?php foreach ($data as $key => $value) {
        echo $value->brand_name;
      } ?></td>
      <td>{{$row->brand_model_name}}</td>

      <td><img src="/images/{{$row->brand_image}}" width="60px" height="60px"></td>
      <td>{{$row->actualprice}}</td>
      <td>{{$row->sellingprice}}</td>
      <td>{{$row->screenproPrice}}</td>
      <td>{!!$row->about!!}</td>
      <td><a href="/admin/brand_edit-model/{{$row->id}}" class="btn btn-info">edit</a></td>
  		</td>
      <td><a href="/admin/brand_model/{{$row->id}}" class="btn btn-danger">Delete</a></td>

	</tr>
  	
  </tbody>
 @empty
    <p style="color: red;">No  Details</p>
  @endforelse 
</table>
	</div>
	</div>



    <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
  <script>
            
            CKEDITOR.replace( 'about' );
            CKEDITOR.replace( 'about' );

</script>
    
@endsection